[![build status](https://gitlab.com/cloud.yantra.oss/microservice-patterns-cqrs/badges/development/build.svg)](https://gitlab.com/cloud.yantra.oss/microservice-patterns-cqrs}) 
[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/2331/badge)](https://bestpractices.coreinfrastructure.org/projects/2331)
[![Coverage Report](https://gitlab.com/cloud.yantra.oss/microservice-patterns-cqrs/badges/development/coverage.svg)](https://cloud.yantra.oss.gitlab.io/microservice-patterns-cqrs/scoverage-report/index.html)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/8a3219564e9b4df88cd4c4c413d52bb6)](https://www.codacy.com/app/cloud-yantra-oss/microservice-patterns-cqrs?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=cloud.yantra.oss/microservice-patterns-cqrs&amp;utm_campaign=Badge_Grade)
[![API docs](https://img.shields.io/badge/api--docs-scala-green.svg)](https://cloud.yantra.oss.gitlab.io/microservice-patterns-cqrs/api/index.html)

# Microservice Patterns - CQRS (Command Query Responsibility Segregation)


A library that would support academic implementation of CQRS pattern for a microservice.

A command will undergo following stages -
- Acceptance
- Technical validation
- Business validation
- Aggregate Mutation or Core domain processing

```mermaid
graph LR;
Acceptance --> T[Technical Validation];
T --> B[Business Validation];
B --> A[Aggregate Mutation]
```

The Command will ever have 3 states, after it has been Accepted - 
- Under processing
- Processed 
  - Success
  - Failure
```mermaid
graph LR;
Accepted --> U[Under Processing];
U --> Success;
U --> Failure;
```
