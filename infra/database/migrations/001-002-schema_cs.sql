REVOKE CREATE ON SCHEMA public FROM PUBLIC;

CREATE SCHEMA IF NOT EXISTS cs
  AUTHORIZATION postgres;

COMMENT ON SCHEMA cs IS 'Command Management Store. 
The Primary Schema for the storing the commands and associated processing stages and statuses for any microservices following the CQRS patterns.';



