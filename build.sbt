
enablePlugins(UniversalPlugin)

/** ****************************************************************************
  * Application related configurations
  * ****************************************************************************
  */
organization := "cloud.yantra.oss"
name := "Microservice CQRS Pattern"
packageName := "microservice-cqrs-pattern"

/** ****************************************************************************
  * Compilation related
  * ****************************************************************************
  */

scalaVersion := "2.12.7"
scalacOptions in (Compile, compile) ++= Seq("-target:jvm-1.8",
  "-unchecked",
  "-deprecation",
  "-encoding", "utf8",
  "-feature",
  "-Ywarn-adapted-args",
  "-Ywarn-dead-code")

//scalacOptions in(Compile, doc) ++= Seq("-groups", "-implicits")

javacOptions in(Compile, compile) ++= Seq("-source", "11",
  "-target", "11",
  "-g:lines")
javacOptions in(Compile, doc) ++= Seq("-notimestamp", "-linksource")

apiURL := Some(url("https://cloud.yantra.oss.gitlab.io/microservice-patterns-cqrs/api/"))

logLevel := sbt.Level.Warn
exportJars := true
libraryDependencies ++= Dependencies.groupBackendDependencies

/** ****************************************************************************
  * Packaging related configurations
  * ****************************************************************************
  */
packageName in Universal := s"${packageName.value}-${version.value}"
exportJars := true

//By default, the dist task will include the API documentation in the generated package.
//Below instruction will exclude them/
//sources in(Compile, doc) := Seq.empty
publishArtifact in(Compile, packageDoc) := false


/** ****************************************************************************
  * CI : Java Checkstyle
  * Ref: https://github.com/etsy/sbt-checkstyle-plugin
  * Usage: sbt chversion in ThisBuild := "2.7.0-SNAPSHOT"eckstyle
  * ****************************************************************************
  */
lazy val javaCheckstyle = "ci/checkstyle/java/google_checks.xml"
lazy val javaCheckstyleHtmlTemplate = "ci/checkstyle/java/checkstyle-noframes.xml"
checkstyleConfigLocation := CheckstyleConfigLocation.File(javaCheckstyle)
//Automatically run checkstyle
(checkstyle in Compile) := (checkstyle in Compile).triggeredBy(compile in Compile).value
checkstyleSeverityLevel := Some(CheckstyleSeverityLevel.Warning)
checkstyleXsltTransformations := {
  Some(Set(CheckstyleXSLTSettings(baseDirectory(_ / javaCheckstyleHtmlTemplate).value, target(_ / "checkstyle-report.html").value)))
}


/** ****************************************************************************
  * CI : Scala Checkstyle
  * Ref: http://www.scalastyle.org/sbt.html
  * Usage: sbt scalastyle
  * ****************************************************************************
  */
lazy val scalaCheckstyle = "ci/checkstyle/scala/scalastyle-config.xml"
scalastyleConfig := baseDirectory(_ / scalaCheckstyle).value
scalastyleFailOnWarning := true

/** ****************************************************************************
  * Test coverage
  * Ref: https://github.com/scoverage/sbt-scoverage
  * ****************************************************************************
  */
coverageEnabled in(Test, compile) := true
coverageEnabled in(Compile, compile) := false
coverageExcludedPackages := "example.*"
coverageMinimum := 70
coverageFailOnMinimum := true
coverageHighlighting := true

/** ****************************************************************************
  * Setting for Akka persistence testing
  * Ref: https://doc.akka.io/docs/akka/2.5/persistence.html?language=scala#testing
  * ****************************************************************************
  */
fork := false
// send output to the build's standard output and error
outputStrategy := Some(StdoutOutput)
resolvers ++= Dependencies.resolvers
parallelExecution in Test := false
testOptions in Test += Tests.Argument("-oF")
