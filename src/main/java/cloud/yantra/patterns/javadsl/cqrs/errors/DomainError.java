package cloud.yantra.patterns.javadsl.cqrs.errors;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * A checked DomainError is represented by this DomainError class.
 * During business processing various errors will be raised, which are not RuntimeException.
 */
public interface DomainError {

    UUID uuid();

    LocalDateTime timestamp();

    String code();

    String message();

    List<String> messageParameters();

    String detail();

    /**
     * returns the formatted message.
     */
    default String getFormattedMessage() {
        return messageParameters().isEmpty()
                ? message()
                : String.format(message(), messageParameters().stream().toArray());
    }
}
