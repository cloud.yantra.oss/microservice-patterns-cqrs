package cloud.yantra.patterns.javadsl.cqrs;

import java.util.UUID;

public final class CommandId {

    private final UUID uuid;

    /**
     * Creates command identifier with fed in UUID.
     *
     * @param uuid Unique identity of command
     */
    public CommandId(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * This method will return the identity of the command.
     *
     * @return identifier of command
     */

    public UUID value() {
        return this.uuid;
    }
}
