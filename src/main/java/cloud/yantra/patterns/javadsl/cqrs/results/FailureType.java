package cloud.yantra.patterns.javadsl.cqrs.results;

public enum FailureType {

    TECHNICAL_VALIDATION("TECHNICAL_VALIDATION"),
    DOMAIN_STATE_VALIDATION("DOMAIN_STATE_VALIDATION"),
    DOMAIN_PROCESSING("DOMAIN_PROCESSING"),
    GENERIC_DOMAIN_ERROR("GENERIC_DOMAIN_ERROR");

    private String value;

    public String getValue() {
        return value;
    }

    FailureType(String value) {
        this.value = value;
    }

}
