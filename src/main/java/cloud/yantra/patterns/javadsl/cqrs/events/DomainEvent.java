package cloud.yantra.patterns.javadsl.cqrs.events;

import cloud.yantra.patterns.javadsl.cqrs.EventId;
import cloud.yantra.patterns.javadsl.cqrs.Version;
import cloud.yantra.patterns.javadsl.cqrs.domain.AggregateId;

public interface DomainEvent {

    /**
     * returns the name of the DomainEvent
     * raised after completion of a business process.
     *
     * @return a name of the concrete implementation applied for this Event
     */
    String name();

    /**
     * returns the ID of the event.
     *
     * @return a UUID for this Event.
     */
    EventId identity();

    /**
     * returns the schema version of the even representing a business process.
     *
     * @return a version of the concrete implementation applied for this event
     */
    default Version version() {
        //Application version can be returned
        return Version.VERSION;
    }

    /**
     * Returns the domain root aggregate ID.
     *
     * @return UUID representation of the domain root aggregate ID
     */
    AggregateId domainRootAggregateId();
}
