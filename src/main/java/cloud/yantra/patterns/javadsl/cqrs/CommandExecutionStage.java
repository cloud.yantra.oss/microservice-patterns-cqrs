package cloud.yantra.patterns.javadsl.cqrs;

public enum CommandExecutionStage {
    // Command has been received
    RECEIVED(1, "Received"),
    // Technical validation has been applied to command
    TECHNICAL_VALIDATION(2, "Technical Validation"),
    // Domain validation has been applied to command
    DOMAIN_STATE_VALIDATION(3, "Domain Validation"),
    // Processing of command has completed
    DOMAIN_PROCESSING(4, "Domain Processing");


    private String stageName;
    private int stageNumber;

    CommandExecutionStage(int stageNumber, String stageName) {
        this.stageName = stageName;
        this.stageNumber = stageNumber;
    }

    public int getStageNumber() {
        return stageNumber;
    }

    public String getStageName() {
        return stageName;
    }
}
