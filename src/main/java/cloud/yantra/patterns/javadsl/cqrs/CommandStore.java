package cloud.yantra.patterns.javadsl.cqrs;

import cloud.yantra.patterns.javadsl.cqrs.events.DomainEvent;
import cloud.yantra.patterns.javadsl.cqrs.results.Result;

import java.util.List;

public interface CommandStore {
    /**
     * This method will stores the stores.
     */
    default void store(Command command,
                       CommandExecutionStage commandExecutionStage, List<Result> results) {
        for (Result result : results) {
            store(command, commandExecutionStage, result);
        }
    }

    void store(Command command,
               CommandExecutionStage commandExecutionStage, Result result);

    void store(Command command,
               CommandExecutionStage commandExecutionStage);


    void store(Command command,
               CommandExecutionStage commandExecutionStage, DomainEvent event);
}
