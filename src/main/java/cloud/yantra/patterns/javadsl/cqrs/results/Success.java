package cloud.yantra.patterns.javadsl.cqrs.results;

public final class Success implements Result {
    private static final Success success = new Success();

    private Success() {
        super();
    }

    public static Success getInstance() {
        return success;
    }

}
