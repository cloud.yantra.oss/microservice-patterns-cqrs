package cloud.yantra.patterns.javadsl.cqrs;

import cloud.yantra.patterns.javadsl.cqrs.events.FailureEvent;
import cloud.yantra.patterns.javadsl.cqrs.events.SuccessEvent;
import scala.util.Either;

@FunctionalInterface
public interface DomainProcessingFunction<E extends FailureEvent, S extends SuccessEvent> {

    /**
     * After processing either a Success or Failure event is returned.
     *
     * @return
     */
    Either<E, S> process();
}
