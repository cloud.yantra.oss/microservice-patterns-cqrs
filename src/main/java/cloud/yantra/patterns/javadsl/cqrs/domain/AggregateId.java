package cloud.yantra.patterns.javadsl.cqrs.domain;

public interface AggregateId {
    /**
     * Returns the identity of the aggregate.
     *
     * @return identifier of aggregate
     */

    String value();
}
