package cloud.yantra.patterns.javadsl.cqrs.events;

import cloud.yantra.patterns.javadsl.cqrs.results.Failure;

import java.util.Optional;

public interface FailureEvent extends DomainEvent {

    /**
     * return the reason of failure.
     *
     * @return optional details
     */
    Optional<Failure> reason();
}
