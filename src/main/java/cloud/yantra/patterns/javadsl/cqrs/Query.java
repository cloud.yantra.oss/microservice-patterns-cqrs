package cloud.yantra.patterns.javadsl.cqrs;

import java.util.Optional;

public interface Query {

    /**
     * return the query that has to be executed.
     * and fit for creating prepared statements.
     *
     * @return a fully formed string with that conforms to requirement of the Query
     */

    Optional<String> queryString();
}
