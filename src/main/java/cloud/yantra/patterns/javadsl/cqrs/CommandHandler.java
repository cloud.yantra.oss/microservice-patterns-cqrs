package cloud.yantra.patterns.javadsl.cqrs;

import cloud.yantra.patterns.javadsl.cqrs.events.FailureEvent;
import cloud.yantra.patterns.javadsl.cqrs.events.SuccessEvent;

import java.util.List;

/**
 * This interface represents a command and the list of methods a command should has.
 */
public interface CommandHandler<C extends Command, E extends FailureEvent, S extends SuccessEvent> {

    /**
     * Returns the list of technical validation rules required by this CommandHandler.
     *
     * @return a concrete list of the technical validation rules applied for this CommandHandler.
     */
    List<ValidationRule> technicalValidationRules(C command);

    /**
     * Returns the list of domain validation rules required by this CommandHandler.
     *
     * @return a concrete list of the domain validation rules applied for this CommandHandler.
     */
    List<ValidationRule> domainValidationRules(C command);

    /**
     * Returns a function that has the business logic for domain processing.
     *
     * @return a function that provides DomainEvent after processing of a Command
     */
    DomainProcessingFunction<E, S> domainProcessing(C command);

    /**
     * Returns the version of the command handler handling the business command.
     *
     * @return a version of the concrete implementation applied for this Command.
     */
    default Version getVersion() {
        return Version.VERSION;
    }

}
