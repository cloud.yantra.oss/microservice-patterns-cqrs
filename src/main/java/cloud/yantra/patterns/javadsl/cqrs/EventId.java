package cloud.yantra.patterns.javadsl.cqrs;

import java.util.UUID;

public final class EventId {

    private final UUID uuid;

    /**
     * Creates event identifier with fed in UUID.
     *
     * @param uuid Unique identity of event
     */
    public EventId(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * This method will return the identity of the event.
     *
     * @return identifier of event
     */

    public UUID value() {
        return this.uuid;
    }
}
