package cloud.yantra.patterns.javadsl.cqrs.results;

public interface Result {

    default boolean isSuccess() {
        return (this instanceof Success);
    }

    default boolean isFailure() {

        return (this instanceof Failure);
    }
}
