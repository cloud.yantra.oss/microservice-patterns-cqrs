package cloud.yantra.patterns.javadsl.cqrs.results;

import cloud.yantra.patterns.javadsl.cqrs.errors.DomainError;

import java.util.Arrays;
import java.util.List;

public final class Failure implements Result {

    private static final boolean FAILURE_RESULT = true;
    private List<DomainError> errors;
    private FailureType failureType;

    private Failure(DomainError error, FailureType failureType) {
        this.errors = Arrays.asList(error);
        this.failureType = failureType;
    }

    private Failure(List<DomainError> errorList, FailureType failureType) {
        this.errors = errorList;
        this.failureType = failureType;
    }

    public static final Failure getInstance(DomainError error, FailureType failureType) {
        return new Failure(error, failureType);
    }

    public static final Failure getInstance(List<DomainError> errors, FailureType failureType) {
        return new Failure(errors, failureType);
    }

    @Override
    public boolean isSuccess() {
        return !FAILURE_RESULT;
    }

    @Override
    public boolean isFailure() {
        return FAILURE_RESULT;
    }

    public List<DomainError> getErrors() {
        return errors;
    }

    public FailureType getFailureType() {
        return failureType;
    }
}