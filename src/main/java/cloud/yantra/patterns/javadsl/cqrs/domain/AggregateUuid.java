package cloud.yantra.patterns.javadsl.cqrs.domain;

import java.util.UUID;

public final class AggregateUuid implements AggregateId {

    private final UUID uuid;

    /**
     * Creates aggregate identifier with fed in UUID.
     * @param uuid Unique identifier of the aggregate
     */
    public AggregateUuid(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public String value() {
        return this.uuid.toString();
    }
}
