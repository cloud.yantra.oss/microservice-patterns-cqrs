package cloud.yantra.patterns.javadsl.cqrs;

public enum Version {
    VERSION("1", "0", "0");

    private String major;
    private String minor;
    private String patch;

    Version(String major, String minor, String patch) {
        this.major = major;
        this.minor = minor;
        this.patch = patch;
    }

    public String getMajor() {
        return major;
    }

    public String getMinor() {
        return minor;
    }

    public String getPatch() {
        return patch;
    }
}
