package cloud.yantra.patterns.javadsl.cqrs;

import cloud.yantra.patterns.javadsl.cqrs.results.Result;

/**
 * This interface represents the validation rules for any CommandHandler.
 */
@FunctionalInterface
public interface ValidationRule {

    /**
     * Required method to apply the validation rule.
     *
     * @return {@code {@link Result }} object with the result values.
     */
    Result validate();
}
