package cloud.yantra.patterns.javadsl.cqrs;



public interface Command {

    /**
     * Returns the name of the command representing a business process.
     *
     * @return a name of the concrete implementation applied for this CommandHandler.
     */
    String name();

    /**
     * Returns the ID of the command.
     *
     * @return a UUID for this CommandHandler.
     */
    CommandId identity();

    /**
     * Returns the version of the command representing a business process.
     *l
     * @return a version of the concrete implementation applied for this Command.
     */
    default Version version() {
        return Version.VERSION;
    }
}
