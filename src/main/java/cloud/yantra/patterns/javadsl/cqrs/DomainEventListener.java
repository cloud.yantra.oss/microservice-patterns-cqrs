package cloud.yantra.patterns.javadsl.cqrs;

import cloud.yantra.patterns.javadsl.cqrs.events.FailureEvent;
import cloud.yantra.patterns.javadsl.cqrs.events.SuccessEvent;
import scala.util.Either;

@FunctionalInterface
public interface DomainEventListener<E extends FailureEvent, S extends SuccessEvent> {

    /**
     * Trait for an consumer to publish to raised domain events.
     *
     * @param event either a Failure or Success event
     */

    void publish(Either<E, S> event);
}
