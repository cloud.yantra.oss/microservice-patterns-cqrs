package cloud.yantra.patterns.javadsl.cqrs.domain;

public interface Aggregate {

    /**
     * Returns the identity to identify the aggregate.
     *
     * @return aggregate identifier
     */
    AggregateId identifier();
}
