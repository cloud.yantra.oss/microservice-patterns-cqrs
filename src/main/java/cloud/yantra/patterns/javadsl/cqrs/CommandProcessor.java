package cloud.yantra.patterns.javadsl.cqrs;

import cloud.yantra.patterns.javadsl.cqrs.errors.DomainError;
import cloud.yantra.patterns.javadsl.cqrs.events.DomainEvent;
import cloud.yantra.patterns.javadsl.cqrs.events.FailureEvent;
import cloud.yantra.patterns.javadsl.cqrs.events.SuccessEvent;
import cloud.yantra.patterns.javadsl.cqrs.results.Failure;
import cloud.yantra.patterns.javadsl.cqrs.results.FailureType;
import cloud.yantra.patterns.javadsl.cqrs.results.Result;
import cloud.yantra.patterns.javadsl.cqrs.results.Success;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.util.Either;
import scala.util.Left;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface CommandProcessor {

    /**
     * Return a handle to store for managing state of commands.
     *
     * @return implementation of command store.
     */

    CommandStore commandStore();

    /**
     * Converts a Failure result to a business Failure event.
     *
     * @param failure result contains all the error that occurred during processing
     * @return
     */

    FailureEvent convertToFailureEvent(Failure failure);

    /**
     * Returns an instance of Logger to logging operational phases.
     *
     * @return
     */
    default Logger getLogger() {
        return LoggerFactory.getLogger(getClass());
    }


    /**
     * perform execution of command with command handler.
     */
    default <C extends Command,
            H extends CommandHandler<C, E, S>,
            E extends FailureEvent,
            S extends SuccessEvent> Either<E, S>
        execute(C command, H commandHandler, Optional<DomainEventListener<E, S>> domainEventListener) {
        getLogger().debug("CommandHandler: Starting executing CommandHandler {}",
                commandHandler);
        //Store Command
        commandStore().store(command, CommandExecutionStage.RECEIVED);
        Result technicalValidationResult = validateRules(command,
                commandHandler.technicalValidationRules(command),
                CommandExecutionStage.TECHNICAL_VALIDATION);
        Result domainValidationResult = technicalValidationResult.isSuccess()
                ? validateRules(command,
                commandHandler.domainValidationRules(command),
                CommandExecutionStage.DOMAIN_STATE_VALIDATION)
                : technicalValidationResult;

        Either<E, S> event;

        if (domainValidationResult.isSuccess()) {
            event = executeDomainPocessing(command, commandHandler);
        } else {
            //Create an FailureEvent
            FailureEvent failureEvent = convertToFailureEvent((Failure) domainValidationResult);
            event = new Left(failureEvent);
        }

        if (domainEventListener.isPresent()) {
            domainEventListener.get().publish(event);
        }
        return event;
    }


    /**
     * perform domain specific processing of command, using a handler in given domain.
     */
    default <C extends Command,
            H extends CommandHandler<C, E, S>,
            E extends FailureEvent,
            S extends SuccessEvent>
        Either<E, S> executeDomainPocessing(C command, H commandHandler) {

        Either<E, S> event = commandHandler.domainProcessing(command).process();

        //Store result before return statement
        DomainEvent domainEvent = event.isRight()
                ? event.right().get()
                : event.left().get();
        commandStore().store(command, CommandExecutionStage.DOMAIN_PROCESSING, domainEvent);
        return event;
    }

    /**
     * perform validation rule command .
     */
    default Result validateRules(Command command, List<ValidationRule> rules,
                                 CommandExecutionStage commandExecutionStage) {

        getLogger().debug("Command: Executing validation rules  {}", rules);

        List<Result> results =
                rules.stream().map(ValidationRule::validate).collect(Collectors.toList());
        List<Failure> failures =
                results.stream().filter(Result::isFailure)
                        .map(result -> (Failure) result).collect(Collectors.toList());
        //Store Domain validation stage result
        commandStore().store(command,
                commandExecutionStage, results);

        return failures.isEmpty()
                ? Success.getInstance()
                : mapFailureErrors(failures,
                commandExecutionStage);
    }

    /**
     * This method will returns the error on command execution failure.
     */
    default Failure mapFailureErrors(List<Failure> failures,
                                     CommandExecutionStage commandExecutionStage) {
        List<DomainError> errors
                = failures
                .stream()
                .flatMap(failure -> failure.getErrors().stream())
                .collect(Collectors.toList());
        FailureType failureType
                = FailureType.GENERIC_DOMAIN_ERROR;
        if (commandExecutionStage == CommandExecutionStage.TECHNICAL_VALIDATION) {
            failureType = FailureType.TECHNICAL_VALIDATION;
        } else if (commandExecutionStage == CommandExecutionStage.DOMAIN_STATE_VALIDATION) {
            failureType = FailureType.DOMAIN_STATE_VALIDATION;
        } else if (commandExecutionStage == CommandExecutionStage.DOMAIN_PROCESSING) {
            failureType = FailureType.DOMAIN_PROCESSING;
        }
        getLogger().debug("Errors of type {} generated by command processing - {}",
                failureType, errors);
        return Failure.getInstance(errors, failureType);
    }
}
