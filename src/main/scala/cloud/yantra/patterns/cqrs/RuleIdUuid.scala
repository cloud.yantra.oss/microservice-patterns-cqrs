/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import java.util.UUID

/**
  * [[UUID]] based implementation of the [[RuleId]]
  *
  * @param uuid
  */
case class RuleIdUuid(uuid: UUID = UUID.randomUUID()) extends RuleId {
  override val value: String = uuid.toString
}
