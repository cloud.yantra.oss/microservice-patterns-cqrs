/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage

trait CommandRepository {

  def logStageResult(command: Command, result: Result, stage: DomainProcessingStage): Unit

  def logCommand(command: Command): Unit
}
