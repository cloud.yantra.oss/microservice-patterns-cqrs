/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import cloud.yantra.patterns.es.EntityId

/**
  * This is a special kind of command that does not cause any change in the system. Its of nature query.
  * Any command carrying this tag MUST not make any change to [[cloud.yantra.patterns.es.Aggregate]],
  * [[cloud.yantra.patterns.es.AggregateRoot]] or [[cloud.yantra.patterns.es.ValueObject]] in the system.
  *
  */
trait QueryCommand extends Command {

  /**
    * Identifies an entity in the domain, that has to process this [[QueryCommand]]
    *
    * @return identity of entity as [[CommandId]]
    *
    **/
  def entityId: EntityId
}
