/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import java.util.UUID

abstract class RuleId {

  /**
    * String representation of the rule identifier
    *
    * @return
    */
  def value: String

}

object RuleId {
  def apply(vl: String): RuleId = new RuleId {
    override val value: String = vl
  }

  def apply(uuid: UUID): RuleId = new RuleIdUuid(uuid)

  def unapply(id: RuleId): Option[String] = Some(id.value)
}
