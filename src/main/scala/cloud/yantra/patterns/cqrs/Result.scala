/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import cloud.yantra.patterns.cqrs.errors.DomainError

sealed trait Result

object NotEvaluated extends Result {
  override def toString: String = "NOT_EVALUATED"
}

case object Passed extends Result {
  override def toString: String = "PASSED"
}

case class Failed(errors: Seq[DomainError]) extends Result

object Result {

  def apply(): Result = NotEvaluated

  def apply(result: String): Result = result match {
    case "PASSED" => Passed
    case _ => NotEvaluated
  }

  def apply(results: Seq[Result]): Result = {
    results
      .filter(r => r.isInstanceOf[Failed])
      .map(_.asInstanceOf[Failed]) match {
      case Nil =>
        Passed
      case failures =>
        val errors: Seq[DomainError] = failures
          .flatMap(f => f.errors)
          .sortWith((a, b) => a.timestamp.getNano > b.timestamp.getNano)
        Failed(errors)
    }
  }

  def apply(error: DomainError): Failed = new Failed(Seq(error))

}
