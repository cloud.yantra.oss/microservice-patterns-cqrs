/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

/**
  * Structure representing validation rules to validate any [[Command]].
  * [[cloud.yantra.patterns.actors.validations.CommandValidation]] will uses a [[Seq]] of [[Rule]] for validating a [[Command]].
  * A [[Command]] is allowed to make changes to any [[cloud.yantra.patterns.es.AggregateRoot]],
  * [[cloud.yantra.patterns.es.Aggregate]] or [[cloud.yantra.patterns.es.ValueObject]] only after the [[Result]] post
  * successful evaluation of associated [[Rule]] in the domain is [[Passed]]
  */
abstract class Rule {

  type RuleName = String

  /**
    * Uniquely identifying the instance of the Rule.
    * Useful for auditing and tracing of the command processing stages.
    *
    * @return
    */
  def id: RuleId

  /**
    * Uniquely name for the [[Rule]].
    * The name of the [[Rule]] should be unique in the domain.
    * Defaults to name of class that implements a [[Rule]]
    *
    * @return
    */
  def name: RuleName = getClass.getCanonicalName

  /**
    * Return a function [[ValidationRule]] that will be used for validating a attribute or set of attributes of a [[Command]].
    *
    * @return
    */
  def validate: ValidationRule
}

object Rule {
  def apply(i: RuleId, n: String, v: ValidationRule): Rule = new Rule() {
    override val id: RuleId = i
    override val name: RuleName = n
    override val validate: ValidationRule = v
  }

  def unapply(r: Rule): Option[(RuleId, String, ValidationRule)] = Some((r.id, r.name, r.validate))
}
