/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import cloud.yantra.patterns.es.Entity

trait CreateCommand[Ent <: Entity] extends Command {

}
