/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import cloud.yantra.patterns.es.Event

/**
  * This is a special kind of [[Command]] that contains an [[Event]].
  * [[Event]] that are raised by a foreign domain, but arriving in the domain for processing will be treated as
  * `Trusted` [[Command]]. The event has already occured , and thus a valid event. It can not be rejected.
  * Thus a [[Command]], which is a direct result of [[Event]], need not undergo any
  * [[cloud.yantra.patterns.actors.validations.TechnicalDomainCommandValidation]] and
  * [[cloud.yantra.patterns.actors.validations.BusinessDomainCommandValidation]]. Instead, may directory hit the
  * domain processing stage of the [[cloud.yantra.patterns.es.Entity]].
  * The command may still be rejected or ignored by the [[cloud.yantra.patterns.es.Entity]]'s manager at the
  * domain processing stage, due to some domain constrains or the present event is of no longer interest to
  * the domain.
  */
trait EventCommand extends Command{

  /**
    * The [[Event]] represented by the [[EventCommand]].
    *
    * @return
    */
  def encapsulatingEvent():Event

}
