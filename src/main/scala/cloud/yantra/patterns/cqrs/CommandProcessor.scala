/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage
import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage.{AggregateMutation, BusinessValidation, TechnicalValidation}

import scala.annotation.tailrec

trait CommandProcessor {

  def commandRepository: CommandRepository

  def process[C <: Command](command: C, commandHandler: CommandHandler[C]): Result = {
    // Store the command
    commandRepository.logCommand(command)
    // Merge all the validation, maintaining the sequence
    val processes: Seq[() => (Result, DomainProcessingStage)] =
      commandHandler.technicalValidations(command).map(f => injectStage(f, TechnicalValidation)) ++
        commandHandler.businessValidation(command).map(f => injectStage(f, BusinessValidation)) :+
        injectStage(commandHandler.domainProcessing(command), AggregateMutation)

    chain(command, processes, NotEvaluated)

  }

  /**
    * A tail recursive function to execute the rules of domain processing
    *
    * @param command
    * @param processes
    * @param init
    * @return
    */
  @tailrec
  private def chain(command: Command, processes: Seq[() => (Result, DomainProcessingStage)], init: Result): Result = processes match {
    case h :: tail =>
      val result = h.apply()
      result match {
        case (Passed, stage) =>
          commandRepository.logStageResult(command, Passed, stage)
          chain(command, tail, Passed)

        case (failed: Failed, stage) =>
          commandRepository.logStageResult(command, failed, stage)
          failed

        case (NotEvaluated, _) =>
          //Do nothing but call itself
          chain(command, processes, NotEvaluated)
      }
    case Nil =>
      init
  }

  /**
    * A helper function to inject stage of the process to processes
    *
    * @param validation
    * @param stage
    * @return
    */
  private def injectStage(validation: () => Result, stage: DomainProcessingStage): () => (Result, DomainProcessingStage) = {
    (() => (validation(), stage))
  }

}
