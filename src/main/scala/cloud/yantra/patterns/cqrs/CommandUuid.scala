/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import java.util.UUID

case class CommandUuid(uuid: UUID = UUID.randomUUID()) extends CommandId {
  override val value: String = uuid.toString
}
