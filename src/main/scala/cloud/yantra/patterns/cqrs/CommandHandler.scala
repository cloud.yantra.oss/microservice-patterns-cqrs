/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import cloud.yantra.patterns.cqrs.Version.DefaultVersion

trait CommandHandler[C <: Command] {

  type AggregateProcessor = () => Result

  def technicalValidations(command: C): Seq[ValidationRule]

  def businessValidation(command: C): Seq[ValidationRule]

  def domainProcessing(command: C): AggregateProcessor

  def version: Version = DefaultVersion

}
