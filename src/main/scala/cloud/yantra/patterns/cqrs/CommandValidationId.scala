/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import java.util.UUID

import cloud.yantra.patterns.es.AggregateId


case class CommandValidationId(uuid: UUID = UUID.randomUUID()) extends AggregateId {
  override val value: String = uuid.toString
}
