/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import cloud.yantra.patterns.es.{Entity, EntityId}

trait UpdateCommand[Ent <: Entity] extends Command {

  /**
    * Identifies the entity in the domain that would process this command.
    *
    * @return identity of entity as [[EntityId]]
    */
  def entityId: EntityId
}
