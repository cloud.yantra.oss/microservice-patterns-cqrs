/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs.stages

trait DomainProcessingStage {
  def name: String
}

object DomainProcessingStage {

  case object Received extends DomainProcessingStage {
    override val name: String = "RECEIVED"
  }

  case object TechnicalValidation extends DomainProcessingStage {
    override val name: String = "TECHNICAL_VALIDATION"
  }

  case object BusinessValidation extends DomainProcessingStage {
    override val name: String = "BUSINESS_VALIDATION"
  }

  case object AggregateMutation extends DomainProcessingStage {
    override val name: String = "AGGREGATE_MUTATION"
  }

  def apply(): DomainProcessingStage = Received

  def apply(stage: String): DomainProcessingStage = stage.toUpperCase match {
    case "TECHNICAL_VALIDATION" => TechnicalValidation
    case "BUSINESS_VALIDATION" => BusinessValidation
    case "AGGREGATE_MUTATION" => AggregateMutation
    case _ => Received
  }

  def unapply(s: DomainProcessingStage): Option[String] = Some(s.name)
}
