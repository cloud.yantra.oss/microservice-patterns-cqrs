/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import java.time.{LocalDateTime, ZoneId}

import cloud.yantra.patterns.cqrs.Version.DefaultVersion

trait Command {

  /**
    * Uniquely identifies an command in the domain.
    *
    * @return identity of command as [[CommandId]]
    *
    **/
  def id: CommandId

  /**
    * A logical name assigned to the command.
    *
    * @return name of the command
    */
  def command: String = getClass.getSimpleName

  /**
    * The version of the schema associated with the [[Command]].
    * Will default of application's global schema version represented by
    * [[cloud.yantra.patterns.cqrs.Version.DefaultVersion]].
    *
    * @return version of the schema
    */
  def version: Version = DefaultVersion

  /**
    * The timestamp when the [[Command]] was generated in [[LocalDateTime]].
    * Stores the information of the [[ZoneId]] as well.
    */
  val capturedAt: LocalDateTime = LocalDateTime.now(ZoneId.of("UTC"))
}

object Command {
  def apply(n: String, i: CommandId, v: Version): Command = new Command() {
    override val command: String = n

    override val id: CommandId = i

    override val version = v
  }

  def unapply(c: Command): Option[(String, CommandId, Version)] = Some((c.command, c.id, c.version))
}
