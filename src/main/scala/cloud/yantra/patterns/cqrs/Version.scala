/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

/**
  * Structure representing the version of a data in semantic format
  */
abstract class Version {

  /**
    * `major` element of the [[Version]]
    *
    * @return
    */
  def major: String

  /**
    * `minor` element of the [[Version]]
    *
    * @return
    */
  def minor: String

  /**
    * `patch` element of the [[Version]]
    *
    * @return
    */
  def patch: String

  /**
    * Semantic representation of the [[Version]]
    *
    * @return
    */
  def value(): String = s"${major}.${minor}.${patch}"
}

object Version {

  /**
    * Provides a default version, as [[DefaultVersion]]
    *
    * @return
    */
  def apply: Version = DefaultVersion

  /**
    * Provides a [[Version]] with `major`, and remaining fields populated with values from [[DefaultVersion]].
    *
    * @param major
    * @return
    */
  def apply(major: String): Version = Version.apply(major, DefaultVersion.minor, DefaultVersion.patch)

  /**
    * Provides a [[Version]] with `major` and `minor`, and remaining fields populated with values from [[DefaultVersion]].
    *
    * @param major
    * @param minor
    * @return
    */
  def apply(major: String, minor: String): Version = Version.apply(major, minor, DefaultVersion.patch)

  /**
    * Provides a [[Version]] with `major`, `minor` and `patch`.
    *
    * @param ma `major` part of [[Version]]
    * @param mi `minor` part of [[Version]]
    * @param pa `patch` part of [[Version]]
    * @return
    */
  def apply(ma: String, mi: String, pa: String): Version = new Version {
    override def major: String = ma

    override def minor: String = mi

    override def patch: String = pa
  }

  def unapply(v: Version): Option[(String, String, String)] = Some((v.major, v.minor, v.patch))

  case object DefaultVersion extends Version {
    override val major: String = "1"

    override val minor: String = "0"

    override val patch: String = "0"
  }

}
