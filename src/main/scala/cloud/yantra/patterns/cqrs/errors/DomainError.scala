/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs.errors

import java.text.MessageFormat
import java.time.LocalDateTime
import java.util.UUID

abstract class DomainError {

  val id: UUID = UUID.randomUUID()

  val timestamp: LocalDateTime = LocalDateTime.now()

  def code: String

  def message: String

  def localizationMessageParameters: Seq[String]

  def detail: String = message

  /**
    * returns the formatted message.
    */
  def getFormattedMessage(): String = localizationMessageParameters match {
    case Nil => message
    case _ =>
      MessageFormat.format(message, localizationMessageParameters :_*)
  }
}


object DomainError {

  object Codes {
    val UNSUPPORTED_REQUEST = "UNSUPPORTED_REQUEST"
  }

  def apply: DomainError = new DomainError() {
    override val code: String = Codes.UNSUPPORTED_REQUEST
    override val message: String = Codes.UNSUPPORTED_REQUEST
    override val localizationMessageParameters: Seq[String] = Nil
  }

  def apply(
             cd: String,
             ttl: String,
             lmp: Seq[String]): DomainError = new DomainError() {
    override val code: String = cd
    override val message: String = ttl
    override val localizationMessageParameters: Seq[String] = lmp
  }

  def apply(uuid: UUID,
            ldt: LocalDateTime,
            cd: String,
            ttl: String,
            lmp: Seq[String],
            dtl: String): DomainError = new DomainError() {
    override val id: UUID = uuid
    override val timestamp: LocalDateTime = ldt
    override val code: String = cd
    override val message: String = ttl
    override val localizationMessageParameters: Seq[String] = lmp
    override val detail: String = dtl
  }

  def unapply(de: DomainError): Option[(UUID,
    LocalDateTime,
    String,
    String,
    Seq[String],
    String)] =
    Some((de.id, de.timestamp, de.code, de.message, de.localizationMessageParameters, de.detail))
}



