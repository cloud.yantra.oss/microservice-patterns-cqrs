/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns

package object cqrs {
  /**
    * [[ValidationRule]] represents a function used in [[Rule]] for validating attributes of [[Command]] during its processing.
    */
  type ValidationRule = () => Result
}
