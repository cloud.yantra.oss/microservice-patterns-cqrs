/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import java.util.UUID

abstract class CommandId {

  /**
    * String representation of the command identifier
    *
    * @return
    */
  def value: String

}

object CommandId {
  /**
    * A command id as [[java.util.UUID]].
    *
    * @return
    */
  def apply(): CommandId = new CommandId() {
    override val value: String = UUID.randomUUID().toString
  }

  /**
    * A command id as simple [[String]]
    *
    * @param id value of command id
    * @return
    */
  def apply(id: String): CommandId = new CommandId() {
    override val value: String = id
  }

  def unapply(c: CommandId): Option[String] = Some(c.value)
}

