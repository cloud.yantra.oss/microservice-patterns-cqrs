/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}
import java.util.Base64

package object tools {
  def objectToBase64[A](obj: A): String = {
    val bo = new ByteArrayOutputStream()
    new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(obj)
    Base64.getEncoder.encodeToString(bo.toByteArray)
  }

  def base64ToObject[A](ecoded: String): Option[A] = {
    val bytes: Array[Byte] = Base64.getDecoder.decode(ecoded)
    try {
      val obj = new ObjectInputStream(new ByteArrayInputStream(bytes)).readObject()
      Some(obj.asInstanceOf[A])
    } catch {
      case _: Exception => None
    }
  }
}
