/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

import java.util.UUID

/**
  * An [[UUID]] based implementation of entity.
  *
  * @param uuid identity of the entity
  */
case class EntityUuid(uuid: UUID = UUID.randomUUID()) extends EntityId {

  override val value: String = uuid.toString
}

