/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

trait EntityId {
  /**
    * String representation of the entity identifier
    *
    * @return
    */
  def value: String

  override def toString: String = value
}

object EntityId {

  def apply(v: String): EntityId = new EntityId() {
    override val value: String = v
  }

  def unapply(id: EntityId): Option[String] = Some(id.value)
}
