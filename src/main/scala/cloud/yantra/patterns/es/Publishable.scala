/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

trait Publishable {
  event: Event =>

  /**
    * Topic to which an [[Event]] should be published.
    *
    * @return
    */
  def topics(): Seq[String]

}
