/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

import cloud.yantra.patterns.cqrs.errors.DomainError

/**
  * This is a special kind of Event that will be raised when there is `No Event` raised.
  * E.g. a [[cloud.yantra.patterns.cqrs.Command]] is applied on an [[Entity]]. However, it resulted into no change
  * of the [[Entity]]. The operation will then raised [[ZeroEvent]].
  * The observing party may reach to this [[ZeroEvent]]
  */

case class ZeroEvent(entityId: Option[EntityId],
                     reasons: Seq[DomainError]) extends Event {
  override val id: EventId = EventUuid()
}
