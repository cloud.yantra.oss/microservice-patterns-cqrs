/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

import java.time.{LocalDateTime, ZoneId}

import cloud.yantra.patterns.cqrs.Version
import cloud.yantra.patterns.cqrs.Version.DefaultVersion

trait Event {

  /**
    * Event identifier [[EventId]] that uniquely identifying the event.
    *
    * @return
    */
  def id: EventId

  /**
    * Name of the event, as understood by the domain.
    *
    * @return
    */
  def event: String = getClass.getSimpleName

  /**
    * The schema [[Version]] of the event.
    * This version places an important role during schema evolution of an [[Aggregate]].
    * [[cloud.yantra.patterns.cqrs.Version.DefaultVersion]] is used, when no specific [[Version]] has been defined in the domain.
    *
    * @return
    */
  def version: Version = DefaultVersion

  /**
    * Identifier [[EntityId]] of the associate [[Entity]], related by this event.
    * All events may not have an association with entity, typically failure events.
    * E.g. Request for aggregate creation failed, and it resulted into a failed [[Event]]
    * containing detailed of [[cloud.yantra.patterns.cqrs.errors.DomainError]]
    *
    * @return
    */
  def entityId: Option[EntityId]

  /**
    * The timestamp when the [[Event]] was generated in [[LocalDateTime]].
    * Stores the information of the [[ZoneId]] as well.
    */
  val generatedOn: LocalDateTime = LocalDateTime.now(ZoneId.of("UTC"))

}

