/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

/**
  * This class represent an entity that will be persisted in the domain.
  */
trait Entity {
  /**
    * Uniquely identifies an entity in the domain.
    *
    * @return identity of entity as [[EntityId]]
    */
  def id: EntityId

  /**
    * The name of the [[Entity]] in this domain.
    *
    * @return name of the entity
    */
  def entity: String

}
