/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

/**
  * This will represent the aggregate in a domain.
  * Every aggregate must be associated with an [[AggregateRoot]].
  * If the parent [[AggregateRoot]] is deleted, then the [[Aggregate]] must be also deleted from the domain.
  *
  */
abstract class Aggregate extends Entity {
  /**
    * Uniquely identifies an aggregate in the domain.
    *
    * @return identity of aggregate as [[AggregateId]]
    */
  def aggregateId: AggregateId

  override def id: EntityId = aggregateId
}
