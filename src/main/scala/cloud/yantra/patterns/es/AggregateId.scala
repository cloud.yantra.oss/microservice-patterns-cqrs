/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

import java.util.UUID

trait AggregateId extends EntityId

object AggregateId {
  /**
    * An aggregate id as [[UUID]].
    *
    * @return
    */
  def apply(): AggregateId = AggregateUuid()

  /**
    * An aggregate id as simple [[String]]
    *
    * @param id value of aggregate id
    * @return
    */
  def apply(id: String): AggregateId = new AggregateId() {
    override val value: String = id
  }
}
