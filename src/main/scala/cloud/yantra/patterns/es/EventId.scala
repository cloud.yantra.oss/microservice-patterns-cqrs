/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

trait EventId {
  /**
    * String representation of the event identifier
    *
    * @return
    */
  def value: String
}

object EventId {
  def apply(): EventId = new EventUuid()

  def apply(vl: String): EventId = new EventId() {
    override val value: String = vl
  }

  def unapply(id: EventId): Option[String] = Some(id.value)
}
