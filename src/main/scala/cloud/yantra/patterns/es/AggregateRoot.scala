/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

/**
  * Following DDD (Domain Driven Design) principle, this will represent the aggregate root that controls all
  * [[Aggregate]] in the domain. Every [[Aggregate]] in the domain MUST be linked to an [[AggregateRoot]].
  *
  */
abstract class AggregateRoot extends Aggregate
  with ETagCompliance {

  /**
    * ETag value of the Root aggregate
    *
    * @return
    */
  def etag: String

  /**
    * List of aggregates associated with the aggregate root.
    *
    * @return
    */
  def aggregates: Seq[Aggregate]

}
