/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

/**
  * [[ValueObject]] will be a special kind of entity, that looks like [[Aggregate]]. But they are not associated with a single [[AggregateRoot]].
  *
  * The [[ValueObject]] are associated with more than one [[Aggregate]] or [[AggregateRoot]].
  * They continue to exist when associated [[Aggregate]] or [[AggregateRoot]] are deleted from domain.
  * They are usually of nature "reference data" from a foreign domain or domain within.
  * E.g.
  * - Currency codes
  * - Country codes
  * - Constants in the payment domain as types of payments e.g.
  *   - UK Faster Payment
  *   - India NEFT
  *   - etc.
  */
abstract class ValueObject extends Entity
