/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

import java.util.UUID

/**
  * An event identifier implementation, that uses [[java.util.UUID]] as identifier
  *
  * @param uuid Idenfier for [[Event]]
  */
case class EventUuid(uuid: UUID = UUID.randomUUID()) extends EventId {
  override val value: String = uuid.toString
}
