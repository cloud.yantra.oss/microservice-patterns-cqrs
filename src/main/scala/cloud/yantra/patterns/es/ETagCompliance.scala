/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

trait ETagCompliance {

  /**
    * String representation of the Etag
    * An ETag uniquely identifies the state of an [[AggregateRoot]], within a finite duration
    *
    * @return
    */
  def etag: String

}
