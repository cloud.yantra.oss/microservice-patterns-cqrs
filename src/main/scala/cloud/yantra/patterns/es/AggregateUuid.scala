/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

import java.util.UUID

/**
  * An [[UUID]] based implementation of aggregate root id.
  *
  * @param uuid identity of the aggregate
  */
case class AggregateUuid(uuid: UUID = UUID.randomUUID()) extends AggregateId {
  override val value: String = uuid.toString
}
