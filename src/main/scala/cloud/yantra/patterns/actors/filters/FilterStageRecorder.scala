/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.filters

import akka.actor.Actor

/**
  * A trait to indicate Actor acting as FilterStage recorder
  */
trait FilterStageRecorder extends Actor{


}
