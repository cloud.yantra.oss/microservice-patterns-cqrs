/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.filters

/**
  * Filter will process only messages that have Filterable trait
  * If something does not have Filterable trait it will just go to DeadLetter
  */
trait Filterable
