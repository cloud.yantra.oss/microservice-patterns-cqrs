/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.filters

import cloud.yantra.patterns.cqrs.Result

trait AuthorizationFilter[Message <: SecureMessage] extends Filter[Message] {

  /**
    * The filter should implement the authorization logic, and ensure that authorization fails when conditions
    * are not met.
    *
    * @param message
    * @return
    */
  def authorize(message: Message): Result

  override def filter(message: Message): Result = authorize(message)
}


