/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.filters

import akka.actor.ActorRef
import cloud.yantra.patterns.cqrs.Result

/**
  * A simple class to control the messages supported by filter stage recorder
  * @param filter
  * @param message
  * @param result
  */
case class FilterStage(filter: ActorRef, message: Filterable, result: Result)
