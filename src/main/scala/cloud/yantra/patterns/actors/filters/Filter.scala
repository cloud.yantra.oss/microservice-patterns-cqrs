/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.filters

import akka.actor.{Actor, ActorRef}
import cloud.yantra.patterns.actors.errors.UnrecognizedMessageError
import cloud.yantra.patterns.cqrs.{Failed, NotEvaluated, Passed, Result}

import scala.reflect.{ClassTag, classTag}

trait Filter[Message <: Filterable] extends Actor {

  implicit val classTagM: ClassTag[Message] = classTag[Message]

  /**
    * The next @Filter in the chain
    *
    * @param filter
    */
  def next: ActorRef

  /**
    * The Filter will apply any filtration logic here
    */
  def filter(message: Message): Result

  /**
    * The Filter will
    *
    * @return
    */
  override def receive: Receive = {
    case message: Message if classTagM.runtimeClass.isInstance(message) =>
      val result = filter(message.asInstanceOf[Message])
      result match {
        case Passed =>
          next ! message
          sendToRecorder(FilterStage(self, message, Passed))

        case failed: Failed =>
          next ! failed
          sendToRecorder(FilterStage(self, message, failed))

        case NotEvaluated =>
          self ! message
          sendToRecorder(FilterStage(self, message, NotEvaluated))

      }

    case message: Filterable =>
      val failed = Failed.apply(List(UnrecognizedMessageError(self, message)))
      next ! failed
      sendToRecorder(FilterStage(self, message, failed))
  }

  /**
    * An optional recorder to record the processing stages of the filter
    *
    * @return
    */
  def recorder: Option[ActorRef]

  /**
    * Send the result of the filer processing to recorder
    *
    * @param result
    */
  private def sendToRecorder(filterStage: FilterStage) = if (recorder.nonEmpty) recorder.get ! filterStage
}
