/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.filters

/**
  * Additional type safety to indicate the messages that needs Authentication or Authorization
  */
trait SecureMessage extends Filterable
