/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.entities

import akka.actor.{ActorLogging, ActorRef}
import akka.cluster.pubsub.{DistributedPubSub, DistributedPubSubMediator}
import akka.persistence.{PersistentActor, RecoveryCompleted, SaveSnapshotSuccess, SnapshotOffer}
import cloud.yantra.patterns.actors.entities.EntityManagerActor.Commands.AuthorizedCommand
import cloud.yantra.patterns.actors.entities.EntityManagerActor.Events.ObservedEvent
import cloud.yantra.patterns.actors.entities.EntityPersistentActor.Commands.{GetEntity, PublishToObserver, PublishToObservers, Stop}
import cloud.yantra.patterns.actors.errors.UnsupportedEntityOperation
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.errors.DomainError
import cloud.yantra.patterns.es._

import scala.util.control.NoStackTrace

abstract class EntityPersistentActor[Ent <: Entity](entityClass: Class[Ent],
                                                    entityId: EntityId = EntityUuid())
  extends PersistentActor
    with ActorLogging {

  type EventHandler = (Option[Ent], Event) => Option[Ent]
  type CommandHandler = (Ent, UpdateCommand[Ent]) => Event

  private var entityState: Option[Ent] = None
  private var entityEvents: Seq[Event] = Nil
  private var eventCount: Int = 0

  private val unhandledEvent: (Ent, Event) => Unit = {
    case (ent, evt) =>
      log.warning("Unhandled event [{}] in [{}] with id [{}]",
        evt.getClass.getCanonicalName,
        ent.getClass.getCanonicalName,
        entityId.value)
  }

  private def generateUnhandledCommandZeroEvent(command: Any): ZeroEvent =
    ZeroEvent(Some(entityId), Seq(UnsupportedEntityOperation(entityClass, Some(entityId), command)))


  private def registerWithDistributedPubSubMediator(): Unit = {
    // Register self to listen to [[UpdateCommand]] destined for [[EntityPersistentActor]].
    // Ref: https://doc.akka.io/docs/akka/2.5/distributed-pub-sub.html
    val mediator = DistributedPubSub(context.system).mediator
    // register to path
    log.debug("Registering self [{}] for listening on commands.", self.path)
    mediator ! DistributedPubSubMediator.Put(self)
  }

  /**
    * Handles all the commands for this [[EntityPersistentActor]].
    * Consumes a state of the [[Entity]] and generates an [[Event]] that should be applied to get state of [[Entity]].
    *
    * @param entity [[Entity]] for which the command is applicable
    * @param cmd    the [[UpdateCommand]] that has to handled by [[Entity]]
    * @return
    */
  private def handleCommand(entity: Ent, cmd: UpdateCommand[Ent]): Event = {
    commandHandlers.get(cmd.getClass) match {
      case Some(commandHandler) =>
        commandHandler.apply(entity, cmd)
      case None =>
        log.warning("Command [{}] cannot be processed by entity [{}], as no handler is provided. ",
          cmd.getClass, persistenceId)
        unhandled(cmd)
        generateUnhandledCommandZeroEvent(cmd)
    }
  }

  private def handleEventAndUpdateState(event: Event): Unit =
  // Get function that can process the event
    eventHandlers
      .get(event.getClass) match {
      case Some(evtHandler) =>
        entityState = evtHandler(entityState, event)
        entityEvents = entityEvents :+ event
        eventCount += 1
        log.debug("#### State of Entity with id [{}] is : {}", persistenceId, entityState.toString)
        log.debug("#### Event applied to entity [{}] is: {}", persistenceId, event)
        entityEvents.foreach(e => log.debug("###### List of events -> {}", e.toString))
      case _ =>
        // Should ideally never reach here
        unhandledEvent(entityState.get, event)
    }


  /**
    * This will be the initial behavior of [[EntityPersistentActor]].
    * Once the [[Entity]] is created/initialized, then it will only entertain update commands.
    *
    * @return
    */
  private def entityCreateCommand: Receive = {
    case AuthorizedCommand(create: CreateCommand[Ent], commandHandler) =>
      log.debug("Received creation command [{}] for processing by [{}].", create, self.path)
      // Get function that can process the received command
      val event: Event = entityInitializationHandler(create)
      event match {
        case ZeroEvent(_, _) =>
          log.warning("Command [{}] cannot be processed by entity [{}] ", create.getClass, persistenceId)

        case e: Event =>
          persist(event)(handleEventAndUpdateState)
          log.debug("Event resulted from command processing is [{}]", event.getClass.getCanonicalName)
          // Now only entertain update command
          context.become(entityUpdateCommand, true)
      }
      sender ! ObservedEvent(event, commandHandler)
      registerWithDistributedPubSubMediator()

    case unhandled =>
      log.error("Received object of type [{}] by entity with reserved id [{}] for creation, with data [{}]",
        unhandled.getClass.getCanonicalName,
        persistenceId,
        unhandled)
      sender ! generateUnhandledCommandZeroEvent(unhandled)
  }

  /**
    * This will be behavior of [[EntityPersistentActor]] after it has been initialized, and remain so, as long as it
    * is active.
    *
    * @return
    */
  private def entityUpdateCommand: Receive = {
    case AuthorizedCommand(GetEntity(_, id, observer), _) =>
      log.debug("Sending [{}] information about entity [{}]", observer.path, entityState)
      if (id == entityId) {
        observer ! entityState.get
      } else {
        observer ! None
      }

    case AuthorizedCommand(PublishToObserver(_, id, observer), _) =>
      self ! GetEntity(CommandId(), id, observer)

    case AuthorizedCommand(PublishToObservers(_, id, observers), _) =>
      observers.foreach(self ! GetEntity(CommandId(), id, _))

    case AuthorizedCommand(update: UpdateCommand[Ent], commandHandler) =>
      log.debug("Received update command [{}] for processing by [{}].", update, self.path)
      // Get function that can process the received command
      val event: Event = handleCommand(entityState.get, update)
      event match {
        case ZeroEvent(_, _) =>
          log.warning("Command [{}] cannot be processed by entity [{}] ", update.getClass, persistenceId)

        case event: Event =>
          log.debug("Event resulted from command processing is [{}]", event.getClass.getCanonicalName)
          persist(event)(handleEventAndUpdateState)
      }
      sender ! ObservedEvent(event, commandHandler)

    // The actor will receive a SaveSnapshotSuccess message when a snapshot was successfully saved.
    // There's no need to do anything, but if we don't handle it,
    // it will pollute the event stream with UnhandledMessage notifications
    case SaveSnapshotSuccess(_) => () // nothing to do

    case Stop =>
      log.info("Stopping the persistent actor with id [{}] and at path [{}]", persistenceId, self.path)
      context.stop(self)

    case unhandled =>
      log.error("Received object of type [{}] by entity with reserved id [{}] for update, with data [{}]",
        unhandled.getClass.getCanonicalName,
        persistenceId,
        unhandled)
      sender ! generateUnhandledCommandZeroEvent(unhandled)
  }

  override val persistenceId: String = EntityPersistentActor.entityPersistenceId(entityClass, entityId)

  /**
    * Start with [[CreateCommand]] acceptance mode, when starting afresh.
    *
    * @return
    */
  override def receiveCommand: Receive = entityCreateCommand

  override def receiveRecover: Receive = {
    case event: Event =>
      log.info("Recovering entity with id [{}] using event : {}", persistenceId, event.toString)
      handleEventAndUpdateState(event)

    case SnapshotOffer(_, snapshot: Entity) =>
      entityState = Some(snapshot.asInstanceOf[Ent])
      log.info("Recovered from snapshot for entity [{}]", persistenceId)

    case RecoveryCompleted =>
      // Depending on the state of the entity, start either in accepting Create or Update commands
      // Create command MUST be only allow when the Entity is created for 1st time.
      entityState match {
        case None => context.become(entityCreateCommand, true)
        case _ => context.become(entityUpdateCommand, true)
      }
      log.debug("State post recovery for id [{}] is : {}", persistenceId, entityState)
      registerWithDistributedPubSubMediator()

    case any: Any =>
      //Do nothing
      log.error("Received object of type [{}] can not used for recovery by [{}] for recovery",
        any.getClass.getCanonicalName,
        this.getClass.getCanonicalName)
  }


  override protected def onPersistFailure(cause: Throwable, event: Any, seqNr: Long): Unit = {
    val domainError: DomainError = DomainError(
      EntityPersistentActor.Errors.PersistException.getClass.getCanonicalName,
      "Persist of [{}] failed in entity with id [{}], caused by: {}",
      Seq(
        event.getClass.getCanonicalName,
        entityId.value,
        cause.getMessage
      ))

    sender ! EntityPersistentActor.Errors.PersistException(domainError)
    super.onPersistFailure(cause, event, seqNr)
  }

  override protected def onPersistRejected(cause: Throwable, event: Any, seqNr: Long): Unit = {
    val domainError: DomainError = DomainError(
      EntityPersistentActor.Errors.PersistException.getClass.getCanonicalName,
      "Persist of [{}] rejected in entity with id [{}], caused by: {}",
      Seq(
        event.getClass.getCanonicalName,
        entityId.value,
        cause.getMessage
      ))
    sender ! EntityPersistentActor.Errors.PersistException(domainError)
    super.onPersistRejected(cause, event, seqNr)
  }

  /**
    * A command handler that will take in a create command and provide a creation success event or
    * [[cloud.yantra.patterns.es.ZeroEvent]] in case of issues.
    *
    * @param command a command with all data required to create the [[Entity]]
    * @return
    */
  def entityInitializationHandler(command: CreateCommand[Ent]): Event

  /**
    * An immutable [[Map]], where key is an concrete implementation of [[Command]],
    * and corresponding value is a function that takes in [[Entity]] and concrete implementation of [[Command]]
    * (i.e. the key) returning a concrete implementation of [[Event]]
    * that can be applied to the [[Entity]] for changing its state.
    *
    * @return
    */
  def commandHandlers: Map[Class[_ <: UpdateCommand[Ent]], CommandHandler]

  /**
    * An immutable [[Map]], where key is an concrete implementation of [[Event]],
    * and corresponding value is a function that takes in [[Entity]] and concrete implementation of [[Event]]
    * (i.e. the key) returning a new state of [[Entity]]
    *
    * @return
    */
  def eventHandlers: Map[Class[_ <: Event], EventHandler]
}

object EntityPersistentActor {

  val IdSeparator: Char = '-'

  def entityPersistenceId(entityClass: Class[_ <: Entity], entityId: EntityId): String =
    s"${entityClass.getSimpleName}${IdSeparator}${entityId.value}"

  /**
    * @return the entity id part encoded in the persistence id
    */
  def extractEntityId(persistenceId: String): String = {
    val idx = persistenceId.indexOf(IdSeparator)
    if (idx > 0) {
      persistenceId.substring(idx + 1)
    } else {
      throw new IllegalArgumentException(
        s"Cannot split '$persistenceId' into persistenceIdPrefix and entityId " +
          s"because there is no separator character ('$IdSeparator')"
      )
    }
  }

  object Errors {

    /**
      * Exception that is used when persist fails.
      */
    final case class PersistException(message: DomainError)
      extends IllegalArgumentException(message.getFormattedMessage())
        with NoStackTrace

  }

  object Commands {

    case class GetEntity(override val id: CommandId,
                         override val entityId: EntityId,
                         observer: ActorRef) extends QueryCommand

    case class PublishToObserver(override val id: CommandId,
                                 override val entityId: EntityId,
                                 observer: ActorRef) extends QueryCommand

    case class PublishToObservers(override val id: CommandId,
                                  override val entityId: EntityId,
                                  observers: Seq[ActorRef]) extends QueryCommand

    case class Stop(entityId: EntityId)

  }

}
