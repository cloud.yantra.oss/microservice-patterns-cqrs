/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.serializer

import java.io.NotSerializableException
import java.time.LocalDateTime
import java.util.UUID

import akka.serialization.SerializerWithStringManifest
import cloud.yantra.patterns.actors.commands.CommandHandlerActor.Events.{CommandProcessingFailed, CommandReceived}
import cloud.yantra.patterns.actors.validations.CommandValidation
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.errors.DomainError
import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage
import cloud.yantra.tools._
import play.api.libs.functional.syntax._
import play.api.libs.json._


class CommandValidationJsonSerializer extends SerializerWithStringManifest {

  implicit val domainErrorWrite: Writes[DomainError] = (
    (__ \ "id").write[UUID] and
      (__ \ "timestamp").write[LocalDateTime] and
      (__ \ "code").write[String] and
      (__ \ "title").write[String] and
      (__ \ "localizationMessageParameters").write[Seq[String]] and
      (__ \ "detail").write[String]
    ) (unlift(DomainError.unapply))

  implicit val domainErrorReads: Reads[DomainError] = (
    (__ \ "id").read[UUID] and
      (__ \ "timestamp").read[LocalDateTime] and
      (__ \ "code").read[String] and
      (__ \ "title").read[String] and
      (__ \ "localizationMessageParameters").read[Seq[String]] and
      (__ \ "detail").read[String]
    ) (DomainError.apply(_, _, _, _, _, _))

  implicit val failedReads = Json.reads[Failed]

  implicit object resultReads extends Reads[Result] {

    override def reads(json: JsValue): JsResult[Result] = json.validate[Failed](failedReads) match {
      case JsSuccess(failedResult: Failed, _) => JsSuccess(failedResult)
      case _ => json.validate[String] match {
        case JsSuccess(result, _) => JsSuccess(Result(result))
        case unknown => JsError(s"Could not process ${unknown} for Result")
      }
    }
  }

  implicit object ruleIdReads extends Reads[RuleId] {
    override def reads(json: JsValue): JsResult[RuleId] = json.validate[String] match {
      case JsSuccess(value, _) => JsSuccess(RuleId(value))
      case unknown => JsError(s"Could not process ${unknown} for RuleId")
    }
  }

  implicit object ruleIdWrites extends Writes[RuleId] {
    override def writes(id: RuleId): JsValue = Json.toJson(id.value)
  }

  implicit object validationRuleReads extends Reads[ValidationRule] {
    override def reads(json: JsValue): JsResult[ValidationRule] = json.validate[String] match {
      case JsSuccess(value, _) => base64ToObject[ValidationRule](value) match {
        case Some(f) => JsSuccess(f)
        case None => JsError(s"Could deserialize ${value} to [cloud.yantra.patterns.ValidationRule]")
      }
      case unknown => JsError(s"Could not process ${unknown} for ValidationRule")
    }
  }

  implicit object validationRuleWrites extends Writes[ValidationRule] {
    override def writes(o: ValidationRule): JsValue = Json.toJson(objectToBase64(o))
  }

  implicit val ruleReads = Json.reads[Rule]

  implicit val ruleWrites = Json.writes[Rule]

  implicit object resultWrites extends Writes[Result] {
    val resultFailedWrites = Json.writes[Failed]

    override def writes(result: Result): JsValue = result match {
      case NotEvaluated => Json.toJson(NotEvaluated.toString)
      case Passed => Json.toJson(Passed.toString)
      case f: Failed => Json.toJson(f)(resultFailedWrites)
    }
  }

  implicit object commandIdReads extends Reads[CommandId] {
    override def reads(json: JsValue): JsResult[CommandId] = json.validate[String] match {
      case JsSuccess(value, _) => JsSuccess(CommandId(value))
      case unknown => JsError(s"Could not process ${unknown} for CommandId")
    }
  }

  implicit object commandIdWrites extends Writes[CommandId] {
    override def writes(o: CommandId): JsValue = Json.toJson(o.value)
  }

  implicit val versionReads: Reads[Version] = (
    (__ \ "major").read[String] and
      (__ \ "minor").read[String] and
      (__ \ "patch").read[String]
    ) (Version.apply(_, _, _))
  implicit val versionWrites: Writes[Version] = (
    (__ \ "major").write[String] and
      (__ \ "minor").write[String] and
      (__ \ "patch").write[String]
    ) (unlift(Version.unapply))

  implicit val commandReads: Reads[Command] = (
    (__ \ "commandName").read[String] and
      (__ \ "id").read[CommandId] and
      (__ \ "version").read[Version]
    ) (Command.apply(_, _, _))
  implicit val commandWrites: Writes[Command] = (
    (__ \ "commandName").write[String] and
      (__ \ "id").write[CommandId] and
      (__ \ "version").write[Version]
    ) (unlift(Command.unapply))

  implicit val domainProcessingStageReads = Json.reads[DomainProcessingStage]
  implicit val domainProcessingStageWrites = Json.writes[DomainProcessingStage]

  implicit val commandValidationInitiatedReads = Json.reads[CommandValidation.CommandValidationInitiated]
  implicit val commandValidationInitiatedWrites = Json.writes[CommandValidation.CommandValidationInitiated]
  implicit val ruleValidationInitiatedReads = Json.reads[CommandValidation.RuleValidationInitiated]
  implicit val ruleValidationInitiatedWrites = Json.writes[CommandValidation.RuleValidationInitiated]
  implicit val ruleValidationCompletedReads = Json.reads[CommandValidation.RuleValidationCompleted]
  implicit val ruleValidationCompletedWrites = Json.writes[CommandValidation.RuleValidationCompleted]


  implicit val commandProcessingFailedReads = Json.reads[CommandProcessingFailed]
  implicit val commandProcessingFailedWrites = Json.writes[CommandProcessingFailed]
  implicit val commandReceivedReads = Json.reads[CommandReceived]
  implicit val commandReceivedWrites = Json.writes[CommandReceived]

  override val identifier: Int = 20181231

  override def manifest(o: AnyRef): String = o.getClass.getCanonicalName

  override def toBinary(o: AnyRef): Array[Byte] = o match {
    case e: CommandValidation.CommandValidationInitiated => Json.toBytes(Json.toJson(e))
    case e: CommandValidation.RuleValidationInitiated => Json.toBytes(Json.toJson(e))
    case e: CommandValidation.RuleValidationCompleted => Json.toBytes(Json.toJson(e))
    case e: CommandProcessingFailed => Json.toBytes(Json.toJson(e))
    case e: CommandReceived => Json.toBytes(Json.toJson(e))
    case unknown =>
      throw new Exception(s"Cannot serialize [${unknown}] with [${this.getClass.getCanonicalName}].")
  }

  override def fromBinary(bytes: Array[Byte], manifest: String): AnyRef = {
    val json = Json.parse(bytes)
    manifest match {
      case "cloud.yantra.patterns.actors.validations.CommandValidation.CommandValidationInitiated" =>
        json.as[CommandValidation.CommandValidationInitiated]
      case "cloud.yantra.patterns.actors.validations.CommandValidation.RuleValidationInitiated" =>
        json.as[CommandValidation.RuleValidationInitiated]
      case "cloud.yantra.patterns.actors.validations.CommandValidation.RuleValidationCompleted" =>
        json.as[CommandValidation.RuleValidationCompleted]
      case "cloud.yantra.patterns.actors.commands.CommandHandlerActor.Events.CommandProcessingFailed" =>
        json.as[CommandProcessingFailed]
      case "cloud.yantra.patterns.actors.commands.CommandHandlerActor.Event.CommandReceived" =>
        json.as[CommandReceived]
      case anything =>
        throw new NotSerializableException(s"Serialization case absent for [${anything}]")
    }
  }
}
