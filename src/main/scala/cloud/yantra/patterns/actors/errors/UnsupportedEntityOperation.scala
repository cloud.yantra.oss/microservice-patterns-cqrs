/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.errors

import cloud.yantra.patterns.cqrs.errors.DomainError
import cloud.yantra.patterns.es.{Entity, EntityId}

/**
  * Domain error that is used when an instruction is received for some action on an [[Entity]].
  *
  * @param entityClass
  * @param entityId
  * @param operation
  */
final case class UnsupportedEntityOperation(entityClass: Class[_ <: Entity],
                                            entityId: Option[EntityId],
                                            operation: Any) extends DomainError {

  override val code: String = classOf[UnsupportedEntityOperation].getSimpleName

  override def message: String = "Unhandled command [{}] received for entity [{}] with id [{}]"

  override def localizationMessageParameters: Seq[String] = Seq(operation.getClass.getSimpleName,
    entityClass.getSimpleName,
    entityId match {
      case Some(id) => id.value
      case None => "undefined"
    }
  )
}
