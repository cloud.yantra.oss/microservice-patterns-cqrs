/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.errors

import cloud.yantra.patterns.actors.commands.CommandHandlerActor
import cloud.yantra.patterns.cqrs.errors.DomainError

/**
  * Domain error that is used when an instruction is received for some action, by a [[CommandHandlerActor]].
  *
  * @param entityId
  * @param operation
  */
final case class UnsupportedOperation(operator: String,
                                      operation: String) extends DomainError {

  override val code: String = classOf[UnsupportedOperation].getSimpleName

  override def message: String = "Unhandled command [{}] received by command handler [{}]."

  override def localizationMessageParameters: Seq[String] = Seq(operation, operator)
}
