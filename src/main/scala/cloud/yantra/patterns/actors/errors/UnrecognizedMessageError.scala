/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.errors

import akka.actor.ActorRef
import cloud.yantra.patterns.actors.filters.Filterable
import cloud.yantra.patterns.cqrs.errors.DomainError

case class UnrecognizedMessageError(filter: ActorRef, filterable: Filterable) extends DomainError {
  override def code: String = "UnrecognizedMessage"

  override def message: String = s"Filter actor [${filter}] is not able to process message [${filterable}]"

  override def localizationMessageParameters: Seq[String] = Seq(filter.toString(), filterable.toString)
}
