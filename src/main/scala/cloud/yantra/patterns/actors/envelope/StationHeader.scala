/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.envelope

import akka.actor.ActorRef

/**
  * Envelope Wrapper Pattern
  */
trait StationHeader {

  type Station = ActorRef

  /**
    * Details about the originator of the message.
    *
    * @return
    */
  def originator(): Option[Station]

  /**
    * Final destination of the message.
    *
    * @return
    */
  def destination(): Seq[Station]

  /**
    * Intermediate stations, which interacted with the message
    */
  def intermediates(): Seq[Station]

  /**
    * List of stations that are interested in the outcomes of message's processing
    * @return
    */
  def resultObservers():Seq[Station]

}
