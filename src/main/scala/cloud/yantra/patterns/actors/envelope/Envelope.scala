/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.envelope

/**
  * Envelope Wrapper Pattern
  */
trait Envelope {

  /**
    * Provides the header element of the envelope
    *
    * @return
    */
  def header(): Option[StationHeader]

  /**
    * Provides the body element of the envelope that carries the message
    *
    * @return
    */
  def body(): Option[Message]

}
