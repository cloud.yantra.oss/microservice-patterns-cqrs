/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.envelope

trait Message {

  type PayLoad

  def payload: Option[PayLoad]

}
