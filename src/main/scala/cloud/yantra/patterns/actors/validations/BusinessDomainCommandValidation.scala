/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.validations

import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage
import cloud.yantra.patterns.cqrs.{Command, CommandId}

/**
  * [[BusinessDomainCommandValidation]] represents a [[CommandValidation]] that will only used for applying
  * [[cloud.yantra.patterns.cqrs.Rule]] that are `stateful` in nature.
  * i.e. these [[cloud.yantra.patterns.cqrs.Rule]] will require access to the state of
  * the [[cloud.yantra.patterns.es.AggregateRoot]] for validating the attributes of [[cloud.yantra.patterns.cqrs.Command]].
  *
  * This domain validation stage that deals with only rules that are associated with [[DomainProcessingStage.BusinessValidation]]
  *
  */
abstract class BusinessDomainCommandValidation[Cmd <: Command](command: CommandId)
  extends CommandValidation[Cmd] {

  override def commandValidationId: String = command.value

  override val domainProcessingStage: DomainProcessingStage = DomainProcessingStage.BusinessValidation
}
