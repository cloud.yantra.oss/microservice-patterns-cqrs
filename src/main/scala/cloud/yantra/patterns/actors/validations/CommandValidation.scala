/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.validations

import akka.actor.{ActorLogging, ActorRef, PoisonPill}
import akka.persistence.{PersistentActor, RecoveryCompleted}
import cloud.yantra.patterns.actors.validations.CommandValidation._
import cloud.yantra.patterns.actors.validations.ValidationRuleWorker.{InitiateValidation, WorkerFinishedValidation, WorkerWaitingInitiation}
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage
import cloud.yantra.patterns.es._

import scala.collection.immutable.HashMap

trait CommandValidation[Cmd <: Command] extends PersistentActor
  with ActorLogging {

  /**
    * Stores the [[Command]] being processed or recovered.
    */
  private var commandState: Option[Cmd] = None

  /**
    * Stores the process state of [[Rule]] that are being processed or recovered.
    */
  private var processingState: Map[RuleId, Result] = HashMap()

  /**
    * Stores the processing stage of this validation actor, as [[DomainProcessingStage]]
    */
  private var processingStage: DomainProcessingStage = domainProcessingStage

  /**
    * [[String]] representing unique identity of this processing stage
    *
    * @return unique identity of the processing stage
    */
  def commandValidationId: String

  /**
    * The [[DomainProcessingStage]] represented by instance of [[CommandValidation]] while processing a [[Command]].
    *
    * @return
    */
  def domainProcessingStage: DomainProcessingStage


  /**
    * A [[Seq]] of [[Rule]] that will be applied to a [[Command]] to validate its fitment for an [[AggregateRoot]].
    *
    * @return
    */
  def rules: Seq[Rule]

  override def persistenceId: String = s"${commandValidationId}-${domainProcessingStage.name}"

  override def receiveCommand: Receive = processDomainCommand

  override def receiveRecover: Receive = {

    case CommandValidationInitiated(_, stage, command) =>
      commandState = Some(command.asInstanceOf[Cmd])
      processingStage = stage
      context.become(processRuleWorkerEvents)

    case RuleValidationInitiated(_, _, rule) =>
      processingState += (rule.id -> NotEvaluated)

    case RuleValidationCompleted(_, _, rule: Rule, result: Result) =>
      processingState += (rule.id -> result)

    case RecoveryCompleted =>
      log.debug("Recovery completed for [{}]!", commandState)
      // Check for any rules that are not Evaluated. If exist, then restart the validation of rules.
      if (isRulesValidationPending) {
        // Resubmit every rule for revaluation, as old results may not hold true, given the process was not completed.
        evaluateRules(rules)
      }

    case any: Any =>
      //Do nothing
      log.error("Received object of type [{}] that can not be handled by [{}]",
        any.getClass.getCanonicalName,
        this.getClass.getCanonicalName)
  }

  /**
    * Initial behaviour of the [[CommandValidation]] when its created.
    * When a [[cloud.yantra.patterns.actors.validations.CommandValidation.ValidateCommand]] command it received,
    * it will start validation of [[Rule]] associated with encapsulating [[Command]].
    *
    * @return
    */
  def processDomainCommand: Receive = {
    case ValidateCommand(command: Cmd) =>
      val commandValidationRequested = CommandValidationInitiated(commandValidationId, domainProcessingStage, command)
      persist(commandValidationRequested) { _ =>
        commandState = Some(command)
        rules match {
          case Nil => self ! GetCommandValidationStatus(context.parent)
          case _ => evaluateRules (rules)
        }
        context.parent ! commandValidationRequested
        context.become(processRuleWorkerEvents)
      }

    case GetCommandValidationStatus(observer) =>
      observer ! NoCommandAvailableForValidation(commandValidationId, domainProcessingStage)
  }

  /**
    * Behaviour of the [[CommandValidation]] after it has started validation.
    * The [[CommandValidation]] will now only entertain the [[Result]] from the [[Rule]] after execution of
    * contained [[ValidationRule]].
    *
    * @return
    */
  def processRuleWorkerEvents: Receive = {

    case WorkerFinishedValidation(rule, result) =>
      persist(RuleValidationCompleted(commandValidationId, domainProcessingStage, rule, result))(_)
      processingState += (rule.id -> result)
      sender ! PoisonPill
      // Inform observer after the last worker has finished
      if (isRulesValidationComplete) {
        self ! GetCommandValidationStatus(context.parent)
      }

    case WorkerWaitingInitiation(rule) =>
      sender ! InitiateValidation
      persist(RuleValidationInitiated(commandValidationId, domainProcessingStage, rule))(_)
      processingState += (rule.id -> NotEvaluated)

    case GetCommandValidationStatus(observer) =>
      log.debug("Sending status of command [{}] to [{}]", commandState, observer.path)
      validationCompletionResult match {
        case NotEvaluated =>
          observer ! ValidationInProgress(
            commandValidationId,
            domainProcessingStage,
            commandState.get,
            NotEvaluated)

        case result: Result =>
          observer ! ValidationCompleted(
            commandValidationId,
            domainProcessingStage,
            commandState.get,
            result
          )
      }

    case ValidateCommand(cmd) =>
      log.warning("Unavailable for validating command [{}] by [{}]",
        cmd.getClass.getCanonicalName,
        this.getClass.getCanonicalName)
      sender ! UnavailableForCommandValidation(commandValidationId, domainProcessingStage)

    case Stop =>
      context.stop(self)
  }

  /**
    * Helper method that takes [[Seq]] of [[Rule]] and spawns [[ValidationRuleWorker]], as child [[akka.actor.Actor]]
    * of [[CommandValidation]].
    * The spawning of the actors are recorded as [[RuleValidationInitiated]] event.
    *
    * @param rules sequence of rules that would be validated
    */
  private def evaluateRules(rules: Seq[Rule]): Unit = {
    rules.foreach {
      rule =>
        val child = context.actorOf(ValidationRuleWorker.props(rule, self))
        persist(RuleValidationInitiated(commandValidationId, domainProcessingStage, rule))(_)
        processingState += (rule.id -> NotEvaluated)
        child ! InitiateValidation
    }
  }

  /**
    * Calculates the [[Result]] of the [[CommandValidation]] stage.
    * Reports as [[Passed]] when every [[Rule]] has reported result as [[Passed]] after evaluation of [[ValidationRule]].
    * Else, will aggregate all the [[cloud.yantra.patterns.cqrs.errors.DomainError]] reported by various [[Rule]] and
    * report [[Failed]].
    *
    * @return
    */
  private def validationCompletionResult: Result = {
    if (isRulesValidationComplete) {
      val domainError: Seq[Result] = processingState.filter {
        case (_, result) => result.isInstanceOf[Failed]
      }
        .toSeq
        .map(_._2)
      Result(domainError)
    } else {
      NotEvaluated
    }
  }

  /**
    * Reports `true` if all [[Rule]] have been validated, else reports as `false` when at least status of one [[Rule]]
    * is [[NotEvaluated]]
    *
    * @return
    */
  private def isRulesValidationComplete: Boolean = !isRulesValidationPending

  /**
    * Reports `false` if any [[Rule]] has not been evaluated i.e. is [[NotEvaluated]].
    *
    * @return
    */
  private def isRulesValidationPending: Boolean = 0 != processingState.count(_._2 == NotEvaluated)

}

object CommandValidation {

  sealed trait CommandValidationCommand

  case class ValidateCommand[Cmd <: Command](command: Cmd) extends CommandValidationCommand

  case class GetCommandValidationStatus(observer: ActorRef) extends CommandValidationCommand

  case object Stop extends CommandValidationCommand

  sealed trait CommandValidationEvent extends Event {
    def commandValidationId: String

    def processingStage: DomainProcessingStage

    override val id: EventId = EventUuid()
    override val event: String = getClass.getCanonicalName
    override val entityId: Option[AggregateId] = Some(AggregateId(s"${}-${processingStage.name}"))
  }

  case class CommandValidationInitiated(commandValidationId: String,
                                        processingStage: DomainProcessingStage,
                                        command: Command) extends CommandValidationEvent

  case class NoCommandAvailableForValidation(commandValidationId: String,
                                             processingStage: DomainProcessingStage) extends CommandValidationEvent

  case class UnavailableForCommandValidation(commandValidationId: String,
                                             processingStage: DomainProcessingStage) extends CommandValidationEvent

  case class RuleValidationInitiated(commandValidationId: String,
                                     processingStage: DomainProcessingStage,
                                     rule: Rule) extends CommandValidationEvent


  case class RuleValidationCompleted(commandValidationId: String,
                                     processingStage: DomainProcessingStage,
                                     rule: Rule,
                                     result: Result) extends CommandValidationEvent

  case class ValidationInProgress(commandValidationId: String,
                                  processingStage: DomainProcessingStage,
                                  command: Command,
                                  result: Result = NotEvaluated) extends CommandValidationEvent

  case class ValidationCompleted(commandValidationId: String,
                                 processingStage: DomainProcessingStage,
                                 command: Command,
                                 result: Result) extends CommandValidationEvent

}
