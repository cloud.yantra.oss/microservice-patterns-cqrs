/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.commands

import cloud.yantra.patterns.actors.commands.CommandHandlerActor.Events.CommandProcessingEvent
import cloud.yantra.patterns.cqrs.{Command, CommandId, Result}

case class CommandProcessingStatus(commandId: CommandId,
                                   command: Option[Command],
                                   status: Result,
                                   events: Seq[CommandProcessingEvent])
