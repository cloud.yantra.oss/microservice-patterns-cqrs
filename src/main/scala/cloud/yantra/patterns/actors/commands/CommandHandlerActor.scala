/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.commands

import java.util.UUID

import akka.actor.{ActorLogging, ActorRef, Props}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Publish
import akka.persistence.{PersistentActor, RecoveryCompleted}
import cloud.yantra.patterns.actors.commands.CommandHandlerActor.Commands.{GetCommandProcessingStatus, ProcessCommand}
import cloud.yantra.patterns.actors.commands.CommandHandlerActor.Events
import cloud.yantra.patterns.actors.commands.CommandHandlerActor.Events._
import cloud.yantra.patterns.actors.entities.EntityManagerActor.Commands.AuthorizedCommand
import cloud.yantra.patterns.actors.errors.UnsupportedOperation
import cloud.yantra.patterns.actors.validations.CommandValidation.{CommandValidationInitiated, Stop, ValidateCommand, ValidationCompleted}
import cloud.yantra.patterns.actors.validations.{BusinessDomainCommandValidation, TechnicalDomainCommandValidation}
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage
import cloud.yantra.patterns.es._


abstract class CommandHandlerActor[Cmd <: Command](commandClass: Class[Cmd],
                                                   commandId: CommandId)
  extends PersistentActor
    with ActorLogging {

  /**
    * Stores the [[Command]] being processed or recovered.
    */
  private var processingCommand: Option[Cmd] = None

  /**
    * Stores the process state of [[Rule]] that are being processed or recovered.
    */
  private var processingEvents: Seq[CommandProcessingEvent] = Nil

  /**
    * Stores the result for [[Command]] being processed or recovered
    */
  private var processingResult: Result = NotEvaluated

  def mediator(): ActorRef = DistributedPubSub(context.system).mediator

  /**
    * A [[Seq]] of [[Rule]] that will be applied to a [[Command]] to validate its fitment for an [[AggregateRoot]].
    *
    * @return
    */
  def technicalDomainCommandValidationRules(cmd: Cmd): Seq[Rule]

  /**
    * A [[Seq]] of [[Rule]] that will be applied to a [[Command]] to validate its fitment for an [[AggregateRoot]].
    *
    * @return
    */
  def businessDomainCommandValidationRules(cmd: Cmd): Seq[Rule]


  /**
    * The topic on which the [[cloud.yantra.patterns.actors.entities.EntityManagerActor]] responsible for
    * [[Entity]] being changed by the [[Command]] handled by this [[CommandHandlerActor]] listens for
    * approved commands (i.e. [[cloud.yantra.patterns.actors.entities.EntityManagerActor.Commands.AuthorizedCommand]])
    *
    * @return
    */
  def entityManagerToValidatedCommandTopic: String

  override def persistenceId: String = commandId.value

  override def receiveRecover: Receive = {
    case event: CommandProcessingEvent =>
      handleCommandProcessingState(event)

    case RecoveryCompleted =>
      log.debug("State post recovery for id [{}] is : {}", persistenceId, processingCommand)
      processingResult match {
        case NotEvaluated =>
          context.become(initializeCommandValidation, false)
        case _ =>
          context.become(queryMode, false)
      }

    case unknown: Any =>
      //Do nothing
      log.error("Received object of type [{}] can not used for recovery by [{}]",
        unknown.getClass.getCanonicalName,
        this.getClass.getCanonicalName)
  }

  override def receiveCommand: Receive = initializeCommandValidation

  private def initializeCommandValidation(): Receive = {

    case ProcessCommand(cmd: Cmd, observer) if commandClass.getCanonicalName == cmd.getClass.getCanonicalName =>
      val receivedEvent = CommandReceived(cmd)
      persist(receivedEvent) { event =>
        log.debug("Stage 1 : Initiating technical validation for command [{}]", cmd)
        val validator = context.actorOf(technicalDomainCommandValidationActor(cmd))
        validator ! ValidateCommand(cmd)
        handleEventAndInternalOperations(observer, event, observeTechnicalDomainValidation(observer))
      }

    case ProcessCommand(unsupported, observer) =>
      val result = Result(UnsupportedOperation(this.getClass.getSimpleName, unsupported.getClass.getSimpleName))
      val generatedEvent = Events.CommandProcessingFailed(commandId,
        DomainProcessingStage.Received,
        result)
      persist(generatedEvent) { event =>
        log.error("Stage 1 : Unsupported command [{}] received", unsupported)
        handleEventAndInternalOperations(observer, event, queryMode)
      }

    case unknown =>
      handleUnrecognizedMessage("Stage 1", unknown)
  }

  private def observeTechnicalDomainValidation(observer: Option[ActorRef]): Receive = {

    case ValidationCompleted(validationId, DomainProcessingStage.TechnicalValidation, cmd: Cmd, Passed) =>
      log.debug("Stage 2 : Technical validation completed with validationId [{}], command [{}] and result [{}]",
        validationId, cmd, Passed)
      val technicalValidationCompleted = TechnicalValidationSuccessful(cmd.id)
      persist(technicalValidationCompleted) { event =>
        //Initiate business validation
        log.debug("Stage 2 : Initiating business validation for command [{}]", cmd)
        handleEventAndInternalOperations(observer, event, observeBusinessDomainValidation(observer))
        val validator = context.actorOf(businessDomainCommandValidationActor(cmd))
        validator ! ValidateCommand(cmd)
        // Technical validation actor is no longer required
        sender ! Stop
      }

    case ValidationCompleted(validationId, DomainProcessingStage.TechnicalValidation, cmd, failed: Failed) =>
      log.debug("Stage 2 : Technical validation completed with validationId [{}], command [{}] and result [{}]",
        validationId, cmd, failed)
      val commandProcessingFailed = CommandProcessingFailed(cmd.id,
        DomainProcessingStage.TechnicalValidation,
        failed)
      persist(commandProcessingFailed) { event =>
        handleEventAndInternalOperations(observer, event, queryMode)
        // Technical validation actor is no longer required
        sender ! Stop
      }

    case msg: CommandValidationInitiated =>
      log.debug("Stage 2 : Technical validation initiated, and event received is [{}]", msg)

    case unknown =>
      handleUnrecognizedMessage("Stage 2", unknown)
  }

  private def observeBusinessDomainValidation(observer: Option[ActorRef]): Receive = {

    case ValidationCompleted(validationId, DomainProcessingStage.BusinessValidation, cmd, Passed) =>
      log.debug("Stage 3 : Business validation completed with validationId [{}], command [{}] and result [{}]",
        validationId, cmd, Passed)
      val businessValidationCompleted = BusinessValidationSuccessful(cmd.id)
      persist(businessValidationCompleted) { event =>
        //Initiate business validation
        log.debug("Stage 3 : Initiating domain validation for command [{}]", cmd)
        // Publish to authorized command topic listened by the entity manager
        log.debug("Stage 3 : Publishing the authorized command for Aggregate mutation to topic [{}]",
          entityManagerToValidatedCommandTopic)
        handleEventAndInternalOperations(observer, event, observeAggregateMutation(observer))
        mediator ! Publish(entityManagerToValidatedCommandTopic, AuthorizedCommand(cmd, Some(self)))
        // Business validation actor is no longer required
        sender ! Stop
      }

    case ValidationCompleted(validationId, DomainProcessingStage.BusinessValidation, cmd, failed: Failed) =>
      // Do something
      log.debug("Stage 3 : Business validation completed with validationId [{}], command [{}] and result [{}]",
        validationId, cmd, failed)
      val commandProcessingFailed = CommandProcessingFailed(cmd.id,
        DomainProcessingStage.BusinessValidation,
        failed)
      persist(commandProcessingFailed) { event =>
        handleEventAndInternalOperations(observer, event, queryMode)
        // Business validation actor is no longer required
        sender ! Stop
      }

    case msg: CommandValidationInitiated =>
      log.debug("Stage 3 : Business validation initiated, and event received is [{}]", msg)

    case unknown =>
      handleUnrecognizedMessage("Stage 3", unknown)
  }

  private def observeAggregateMutation(observer: Option[ActorRef]): Receive = {

    case event@ZeroEvent(_, reasons) =>
      log.debug("Stage 4 : ZeroEvent received post aggregate mutation phase as : [{}]", event)
      val commandProcessingFailed = CommandProcessingFailed(commandId,
        DomainProcessingStage.AggregateMutation,
        Failed(reasons))
      persist(commandProcessingFailed) { event =>
        handleEventAndInternalOperations(observer, event, queryMode)
      }

    case event: Event =>
      log.debug("Stage 4 : Success event received post aggregate mutation phase as : [{}]", event)
      val aggregateMutationSuccessful = AggregateMutationSuccessful(commandId,
        event.entityId.get)
      persist(aggregateMutationSuccessful) { event =>
        handleEventAndInternalOperations(observer, event, queryMode)
      }

    case unknown =>
      handleUnrecognizedMessage("Stage 4", unknown)
  }

  private def queryMode: Receive = {

    case GetCommandProcessingStatus(observer) =>
      log.debug("Stage 5 : Sending status [{}] for the command [{}] to [{}]", processingResult,
        processingCommand,
        observer.path)
      observer ! CommandProcessingStatus(commandId, processingCommand, processingResult, processingEvents)

    case unknown =>
      handleUnrecognizedMessage("Stage 5", unknown)

  }

  private def handleEventAndInternalOperations(observer: Option[ActorRef],
                                               event: CommandProcessingEvent,
                                               nature: Receive): Unit = {
    handleCommandProcessingState(event)
    observer.map(_ ! event)
    context.become(nature, true)
  }

  private def technicalDomainCommandValidationActor(cmd: Cmd): Props =
    Props(new TechnicalDomainCommandValidation[Cmd](commandId) {
      override def rules: Seq[Rule] = technicalDomainCommandValidationRules(cmd)
    })

  private def businessDomainCommandValidationActor(cmd: Cmd): Props =
    Props(new BusinessDomainCommandValidation[Cmd](commandId) {
      override def rules: Seq[Rule] = businessDomainCommandValidationRules(cmd)
    })

  private def handleCommandProcessingState(event: CommandProcessingEvent): Unit = event match {
    case CommandReceived(command: Cmd) =>
      processingCommand = Some(command)
      processingEvents :+ event
      processingResult = NotEvaluated

    case TechnicalValidationSuccessful(_) =>
      processingEvents :+ event
      processingResult = NotEvaluated

    case BusinessValidationSuccessful(_) =>
      processingEvents :+ event
      processingResult = NotEvaluated

    case AggregateMutationSuccessful(_, _) =>
      processingEvents :+ event
      processingResult = Passed

    case event@CommandProcessingFailed(_, _, failed) =>
      processingEvents :+ event
      processingResult = failed

    case unknown =>
      //Should never reach here
      log.error("Unknown event received for handling by [{}] with data [{}]",
        self.path,
        unknown)
  }

  private def handleUnrecognizedMessage(stage: String, unknown: Any): Unit = {
    log.error("Unrecognized message received by [{}] for handling at stage [{}] with data : [{}] ",
      self.path,
      stage,
      unknown)
  }
}


object CommandHandlerActor {

  object Commands {

    sealed trait CommandProcessingCommand

    case class ProcessCommand[Cmd <: Command](command: Cmd,
                                              observer: Option[ActorRef]) extends CommandProcessingCommand

    case class GetCommandProcessingStatus(observer: ActorRef) extends CommandProcessingCommand

  }

  object Events {

    sealed trait CommandProcessingEvent extends Event {
      def stageId: UUID

      def commandId: CommandId

      def processingStage: DomainProcessingStage

      override def entityId: Option[AggregateId] = Some(AggregateId(commandId.value))

      override def id: EventId = EventUuid(stageId)

      override def event: String = getClass.getCanonicalName
    }

    case class CommandReceived(command: Command) extends CommandProcessingEvent {

      override def commandId: CommandId = command.id

      override val stageId: UUID = UUID.randomUUID()

      override val processingStage: DomainProcessingStage = DomainProcessingStage.Received
    }

    case class TechnicalValidationSuccessful(commandId: CommandId) extends CommandProcessingEvent {

      override def entityId: Option[AggregateId] = Some(AggregateId(commandId.value))

      override val stageId: UUID = UUID.randomUUID()

      override val processingStage: DomainProcessingStage = DomainProcessingStage.TechnicalValidation
    }

    case class BusinessValidationSuccessful(commandId: CommandId) extends CommandProcessingEvent {

      override val stageId: UUID = UUID.randomUUID()

      override val processingStage: DomainProcessingStage = DomainProcessingStage.BusinessValidation
    }


    case class AggregateMutationSuccessful(commandId: CommandId,
                                           affectedDomainEntityId: EntityId) extends CommandProcessingEvent {

      override val stageId: UUID = UUID.randomUUID()

      override val processingStage: DomainProcessingStage = DomainProcessingStage.AggregateMutation
    }

    case class CommandProcessingFailed(commandId: CommandId,
                                       processingStage: DomainProcessingStage,
                                       result: Failed
                                      ) extends CommandProcessingEvent {

      override val stageId: UUID = UUID.randomUUID()

      override def entityId: Option[AggregateId] = Some(AggregateId(commandId.value))
    }

  }

}
