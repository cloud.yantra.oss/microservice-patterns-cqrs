/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.commands

import cloud.yantra.patterns.cqrs.{Command, CommandId}
import example.domain.Address

case class NewCustomerRegistration(id: CommandId,
                                   customerName: String,
                                   address: Address) extends Command {

  override val command: String = NewCustomerRegistration.getClass.getCanonicalName

}
