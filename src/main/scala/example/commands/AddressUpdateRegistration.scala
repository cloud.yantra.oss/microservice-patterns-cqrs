/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.commands

import java.util.UUID

import cloud.yantra.patterns.cqrs.{Command, CommandId}

case class AddressUpdateRegistration(id: CommandId,
                                     customerId: UUID,
                                     lines: Seq[String],
                                     city: String,
                                     country: String) extends Command {

  override val command: String = AddressUpdateRegistration.getClass.getCanonicalName

}
