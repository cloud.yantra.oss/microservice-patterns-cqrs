/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example

import cloud.yantra.patterns.cqrs._
import example.commands.NewCustomerRegistration
import example.domain.Address
import example.handlers.NewCustomerRegistrationHandler


object CommandProcessingEngine {

  def main(): Unit = {

    val newCustomerRegistration = NewCustomerRegistration(
      id = CommandUuid(),
      customerName = "Joe McLarain",
      address = Address(
        Seq("Line One"),
        "Manchester",
        "UK")
    )

    val result: Result = CustomerDomainCommandProcessor.process(newCustomerRegistration, NewCustomerRegistrationHandler)
    result match {
      case Passed =>
        print(s"Customer [${newCustomerRegistration}] was successfully created.")
      case f: Failed =>
        print(s"Customer creation failed because of errors -> [${f.errors.mkString("\n")}]")
      case NotEvaluated =>
      //Do nothing
    }
  }
}

