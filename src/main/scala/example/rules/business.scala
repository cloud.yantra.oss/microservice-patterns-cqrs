/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.rules

import java.util.UUID

import cloud.yantra.patterns.cqrs.{Passed, Result, ValidationRule}
import example.errors.CustomerDoesNotExist
import example.repository.CustomerManagementService

package object business {

  def isCustomerRegistered(id: UUID, repo: CustomerManagementService): ValidationRule =
    () => repo.findCustomerById(id) match {
      case None =>
        Result.apply(CustomerDoesNotExist(id))
      case Some(c) =>
        Passed
    }

}
