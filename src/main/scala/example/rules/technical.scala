/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.rules

import cloud.yantra.patterns.cqrs.{Passed, Result, ValidationRule}
import example.domain.Address
import example.errors.{CountryNotServiced, CustomerAddressLinesAreAbsent, CustomerNameNotProvided}

package object technical {

  private val SERVICED_COUNTRIES = Seq("IE", "UK")

  def addressPresentInServicedCountry(country: String): ValidationRule = () => {
    if (!country.isEmpty && country.length == 2 && SERVICED_COUNTRIES.contains(country)) {
      Passed
    } else {
      Result.apply(CountryNotServiced(country))
    }
  }

  def customerFullNameIsProvided(customerFullName: String): ValidationRule = () => {
    if (customerFullName.isEmpty) {
      Result.apply(CustomerNameNotProvided())
    } else {
      Passed
    }
  }

  def customerAddressLineIsProvided(address: Address): ValidationRule = () => {
    address.lines match {
      case Nil => Result.apply(CustomerAddressLinesAreAbsent())
      case _ => Passed
    }
  }

}
