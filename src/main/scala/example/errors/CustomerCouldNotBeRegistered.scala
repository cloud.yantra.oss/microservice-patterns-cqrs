/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.errors

import cloud.yantra.patterns.cqrs.errors.DomainError

case class CustomerCouldNotBeRegistered(name: String) extends DomainError {

  override def code: String = "CUSTOMER_COULD_BE_REGISTERED"

  override def message: String = s"Customer with name [${name}] could not be registered."

  override def localizationMessageParameters: Seq[String] = Seq(name)

}
