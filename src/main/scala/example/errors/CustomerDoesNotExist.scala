/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.errors

import java.util.UUID

import cloud.yantra.patterns.cqrs.errors.DomainError

case class CustomerDoesNotExist(customerId: UUID) extends DomainError {

  override def code: String = "CUSTOMER_DOES_NOT_EXIST"

  override def message: String = s"Customer with id [${customerId}] does not exist."

  override def localizationMessageParameters: Seq[String] = Seq(customerId.toString)

}
