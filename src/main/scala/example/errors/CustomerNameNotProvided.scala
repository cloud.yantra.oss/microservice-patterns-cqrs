/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.errors

import cloud.yantra.patterns.cqrs.errors.DomainError

case class CustomerNameNotProvided() extends DomainError {

  override val code: String = "CUSTOMER_NAME_NOT_PROVIDED"

  override val message: String = "Customer name is not provided."

  override val localizationMessageParameters: Seq[String] = Nil
}
