/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.errors

import cloud.yantra.patterns.cqrs.errors.DomainError

case class CustomerAddressLinesAreAbsent() extends DomainError {

  override def code: String = "CUSTOMER_ADDRESS_LINES_ARE_ABSENT"

  override def message: String = "Customer address lines are not provided"

  override def localizationMessageParameters: Seq[String] = Nil

}
