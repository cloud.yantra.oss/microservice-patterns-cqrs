/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.errors

import java.time.LocalDateTime
import java.util.UUID

import cloud.yantra.patterns.cqrs.errors.DomainError

case class CountryNotServiced(country: String) extends DomainError {

  override val id: UUID = UUID.randomUUID()

  override val timestamp: LocalDateTime = LocalDateTime.now()

  override val code: String = "COUNTRY_NOT_SERVICED"

  override val message: String = s"Country [${country}] is not serviced."

  override val localizationMessageParameters: Seq[String] = Seq(country)

  override val detail: String = message
}
