/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.handlers

import java.util.UUID

import cloud.yantra.patterns.cqrs._
import example.commands.NewCustomerRegistration
import example.domain.Customer
import example.errors.CustomerCouldNotBeRegistered
import example.repository.CustomerManagementInMemory
import example.rules.technical

object NewCustomerRegistrationHandler extends CommandHandler[NewCustomerRegistration] {

  val customerRepository = CustomerManagementInMemory.apply()

  override def technicalValidations(command: NewCustomerRegistration): Seq[ValidationRule] =
    Seq(technical.customerFullNameIsProvided(command.customerName),
      technical.addressPresentInServicedCountry(command.address.country))

  override def businessValidation(command: NewCustomerRegistration): Seq[ValidationRule] =
    technicalValidations(command)

  override def domainProcessing(command: NewCustomerRegistration): AggregateProcessor = () => {
    val customer = Customer(id = UUID.randomUUID(),
      name = command.customerName,
      address = command.address)

    val result = customerRepository.add(customer) match {
      case None =>
        Result.apply(CustomerCouldNotBeRegistered(command.customerName))
      case Some(c) =>
        Passed
    }

    result
  }

}
