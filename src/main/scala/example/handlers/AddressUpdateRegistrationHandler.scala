/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.handlers

import cloud.yantra.patterns.cqrs._
import example.commands.AddressUpdateRegistration
import example.domain.Address
import example.errors.CustomerDoesNotExist
import example.repository.CustomerManagementService
import example.rules.{business, technical}

case class AddressUpdateRegistrationHandler(repo: CustomerManagementService) extends CommandHandler[AddressUpdateRegistration] {

  override def technicalValidations(command: AddressUpdateRegistration): Seq[ValidationRule]
  = Seq(technical.addressPresentInServicedCountry(command.country))

  override def businessValidation(command: AddressUpdateRegistration): Seq[ValidationRule]
  = Seq(business.isCustomerRegistered(command.customerId, repo))

  override def domainProcessing(command: AddressUpdateRegistration): AggregateProcessor = () => {
    repo.findCustomerById(command.customerId) match {
      case Some(customer) =>
        //Do something
        val address = Address(command.lines, command.city, command.country)
        customer.copy(address = address)
        repo.update(customer)
        Passed
      case None =>
        Result.apply(CustomerDoesNotExist(command.customerId))
    }

  }

}
