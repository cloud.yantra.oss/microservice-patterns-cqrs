/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.repository

import java.util.UUID

import example.domain.Customer

import scala.collection.mutable

class CustomerManagementInMemory extends CustomerManagementService {

  var database = mutable.HashMap[UUID, Customer]()

  override def add(customer: Customer): Option[Customer] = findCustomerById(customer.id) match {
    case None => database.put(customer.id, customer)
    case Some(c) => None
  }

  override def update(customer: Customer): Option[Customer] = findCustomerById(customer.id) match {
    case None => None
    case Some(c) => database.put(c.id, c.copy(id = customer.id,
      name = customer.name,
      address = customer.address
    ))
  }

  override def findCustomerById(id: UUID): Option[Customer] = database.get(id)
}

object CustomerManagementInMemory {

  val customerManagement = new CustomerManagementInMemory()

  def apply(): CustomerManagementInMemory = customerManagement

}
