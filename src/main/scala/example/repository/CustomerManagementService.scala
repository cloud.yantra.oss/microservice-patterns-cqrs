/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.repository

import java.util.UUID

import example.domain.Customer

trait CustomerManagementService {

  def add(customer: Customer): Option[Customer]

  def update(customer: Customer): Option[Customer]

  def findCustomerById(id: UUID): Option[Customer]

}
