/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.domain

import java.util.UUID

case class Customer(id: UUID, name: String, address: Address)
