/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example.domain

case class Address(lines: Seq[String], city: String, country: String)
