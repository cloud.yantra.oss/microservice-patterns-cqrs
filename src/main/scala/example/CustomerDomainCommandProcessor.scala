/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package example

import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage
import cloud.yantra.patterns.cqrs.{Command, CommandProcessor, CommandRepository, Result}

object CustomerDomainCommandProcessor extends CommandProcessor {

  var commandStore = Seq[(Command, Result, DomainProcessingStage)]()

  override def commandRepository: CommandRepository = new CommandRepository {
    override def logStageResult(command: Command, result: Result, stage: DomainProcessingStage): Unit = {}

    override def logCommand(command: Command): Unit = {}
  }
}
