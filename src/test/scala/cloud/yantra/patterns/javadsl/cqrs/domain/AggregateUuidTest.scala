/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.javadsl.cqrs.domain

import java.util.UUID

import org.scalatest.FlatSpec

class AggregateUuidTest extends FlatSpec {

  "Aggregate UUID generator" should ("Should create unique UUID on every creation request") in {

    val aggregateId1 = new AggregateUuid(UUID.randomUUID())
    val aggregateId2 = new AggregateUuid(UUID.randomUUID())
    assert(aggregateId1.value != aggregateId2.value)
  }

  it should ("Must return same value when invoked multiple times for value") in {
    val aggregateId = new AggregateUuid(UUID.randomUUID())
    val id1 = aggregateId.value
    val id2 = aggregateId.value
    assert(id1 == id2)
  }
}
