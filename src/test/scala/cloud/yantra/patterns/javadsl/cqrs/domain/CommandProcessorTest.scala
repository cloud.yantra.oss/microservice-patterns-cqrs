/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.javadsl.cqrs.domain

import java.time.LocalDateTime
import java.util
import java.util.{Collections, Optional, UUID}

import cloud.yantra.patterns.javadsl.cqrs._
import cloud.yantra.patterns.javadsl.cqrs.errors.DomainError
import cloud.yantra.patterns.javadsl.cqrs.events.{FailureEvent, SuccessEvent}
import cloud.yantra.patterns.javadsl.cqrs.results.{Failure, FailureType, Success}
import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec


class CommandProcessorTest extends FlatSpec with MockFactory {

  case class TestFailureEvent(failure: Failure = null) extends FailureEvent {
    override def reason(): Optional[Failure] = Optional.ofNullable(failure)

    override def name(): String = "TestFailure"

    override def identity(): EventId = new EventId(UUID.randomUUID())

    override def domainRootAggregateId(): AggregateId = new AggregateUuid(UUID.randomUUID())
  }

  case class TestSuccessEvent() extends SuccessEvent {
    override def name(): String = "SuccessEvent"

    override def identity(): EventId = new EventId(UUID.randomUUID())

    override def domainRootAggregateId(): AggregateId = new AggregateUuid(UUID.randomUUID())
  }

  class TestCommandProcessor extends CommandProcessor {

    override def commandStore(): CommandStore = stub[CommandStore]

    override def convertToFailureEvent(failure: Failure) = TestFailureEvent(failure)
  }


  lazy val testFailureEvent = new TestFailureEvent()

  val testSuccessEvent = new TestSuccessEvent()

  val commandProcessor = new TestCommandProcessor()

  case class TestCommand() extends Command {
    override def name: String = this.getClass.getName

    override def identity: CommandId = new CommandId(UUID.randomUUID())
  }

  val ERROR_TECHNICAL_VALIDATION_1 = new DomainError {
    override val code: String = "DETE1"

    override val uuid: UUID = UUID.randomUUID()

    override val timestamp: LocalDateTime = LocalDateTime.now()

    override val message: String = code

    override val messageParameters: util.List[String] = Collections.emptyList()

    override val detail: String = "Domain technical validation error 1"

  }

  val ERROR_TECHNICAL_VALIDATION_2 = new DomainError {
    override val code: String = "DETE2"

    override val uuid: UUID = UUID.randomUUID()

    override val timestamp: LocalDateTime = LocalDateTime.now()

    override val message: String = code

    override val messageParameters: util.List[String] = Collections.emptyList()

    override val detail: String = "Domain technical validation error 2"
  }

  val ERROR_BUSINESS_VALIDATION_1 = new DomainError {
    override val code: String = "BTE1"

    override val uuid: UUID = UUID.randomUUID()

    override val timestamp: LocalDateTime = LocalDateTime.now()

    override val message: String = code

    override val messageParameters: util.List[String] = Collections.emptyList()

    override val detail: String = "Business domain validation error 1"
  }

  val ERROR_BUSINESS_VALIDATION_2 = new DomainError {
    override val code: String = "BTE2"

    override val uuid: UUID = UUID.randomUUID()

    override val timestamp: LocalDateTime = LocalDateTime.now()

    override val message: String = code

    override val messageParameters: util.List[String] = Collections.emptyList()

    override def detail: String = "Business domain validation error 2"
  }

  val ERROR_BUSSINESS_PROCESSING = new DomainError {
    override def code: String = "BPE"

    override val uuid: UUID = UUID.randomUUID()

    override val timestamp: LocalDateTime = LocalDateTime.now()

    override val message: String = code

    override val messageParameters: util.List[String] = Collections.emptyList()

    override def detail: String = "Business processing error"
  }

  val VALIDATION_POSITIVE: ValidationRule =
    () => Success.getInstance()
  val VALIDATION_TECHNICAL_NEGATIVE_1: ValidationRule =
    () => Failure.getInstance(ERROR_TECHNICAL_VALIDATION_1,
      FailureType.TECHNICAL_VALIDATION)
  val VALIDATION_TECHNICAL_NEGATIVE_2: ValidationRule =
    () => Failure.getInstance(ERROR_TECHNICAL_VALIDATION_2,
      FailureType.TECHNICAL_VALIDATION)
  val VALIDATION_BUSINESS_NEGATIVE_1: ValidationRule =
    () => Failure.getInstance(ERROR_BUSINESS_VALIDATION_1,
      FailureType.DOMAIN_STATE_VALIDATION)
  val VALIDATION_BUSINESS_NEGATIVE_2: ValidationRule =
    () => Failure.getInstance(ERROR_BUSINESS_VALIDATION_2,
      FailureType.DOMAIN_STATE_VALIDATION)
  val PROCESSING_BUSINESS_NEGATIVE: DomainProcessingFunction[TestFailureEvent, TestSuccessEvent] =
    () => Left(new TestFailureEvent(Failure.getInstance(ERROR_BUSSINESS_PROCESSING,
      FailureType.DOMAIN_STATE_VALIDATION)))


  "Command processor" should "fail technical validation" in {

    val command = TestCommand()
    class TestCommandHandler extends CommandHandler[TestCommand, TestFailureEvent, TestSuccessEvent] {
      override def technicalValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE, VALIDATION_TECHNICAL_NEGATIVE_1)

      override def domainValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_BUSINESS_NEGATIVE_1)

      override def domainProcessing(command: TestCommand): DomainProcessingFunction[TestFailureEvent, TestSuccessEvent] =
        () => Right(testSuccessEvent)
    }

    val commandHandler = new TestCommandHandler()
    val result = commandProcessor.execute[TestCommand,
      TestCommandHandler,
      TestFailureEvent,
      TestSuccessEvent](command, commandHandler, Optional.empty())
    result match {
      case Left(failure) =>
        assert(failure.reason().get().getErrors.contains(ERROR_TECHNICAL_VALIDATION_1))
      case _ => fail()
    }

  }

  it should "collect all technical validations" in {

    val command = TestCommand()
    class TestCommandHandler extends CommandHandler[TestCommand, TestFailureEvent, TestSuccessEvent] {
      override def technicalValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE, VALIDATION_TECHNICAL_NEGATIVE_2, VALIDATION_TECHNICAL_NEGATIVE_1)

      override def domainValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_BUSINESS_NEGATIVE_1)

      override def domainProcessing(command: TestCommand): DomainProcessingFunction[TestFailureEvent, TestSuccessEvent] =
        () => Right(testSuccessEvent)
    }

    val commandHandler = new TestCommandHandler()
    val result = commandProcessor.execute[TestCommand,
      TestCommandHandler,
      TestFailureEvent,
      TestSuccessEvent](command, commandHandler, Optional.empty())
    result match {
      case Left(failure) =>
        assert(failure.reason().get().getErrors.contains(ERROR_TECHNICAL_VALIDATION_1))
        assert(failure.reason().get().getErrors.contains(ERROR_TECHNICAL_VALIDATION_2))
      case _ => fail()
    }
  }

  it should "fail business validation" in {
    val command = TestCommand()
    class TestCommandHandler extends CommandHandler[TestCommand, TestFailureEvent, TestSuccessEvent] {
      override def technicalValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE)

      override def domainValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE, VALIDATION_POSITIVE, VALIDATION_BUSINESS_NEGATIVE_1)

      override def domainProcessing(command: TestCommand): DomainProcessingFunction[TestFailureEvent, TestSuccessEvent] =
        () => Right(testSuccessEvent)
    }

    val commandHandler = new TestCommandHandler()
    val result = commandProcessor.execute[TestCommand,
      TestCommandHandler,
      TestFailureEvent,
      TestSuccessEvent](command, commandHandler, Optional.empty())
    result match {
      case Left(failure) =>
        assert(failure.reason().get().getErrors.contains(ERROR_BUSINESS_VALIDATION_1))
      case _ => fail()
    }
  }

  it should "collect all business validations" in {
    val command = TestCommand()
    class TestCommandHandler extends CommandHandler[TestCommand, TestFailureEvent, TestSuccessEvent] {
      override def technicalValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE)

      override def domainValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE, VALIDATION_BUSINESS_NEGATIVE_2, VALIDATION_BUSINESS_NEGATIVE_1)

      override def domainProcessing(command: TestCommand): DomainProcessingFunction[TestFailureEvent, TestSuccessEvent] =
        () => Right(testSuccessEvent)
    }

    val commandHandler = new TestCommandHandler()
    val result = commandProcessor.execute[TestCommand,
      TestCommandHandler,
      TestFailureEvent,
      TestSuccessEvent](command, commandHandler, Optional.empty())
    result match {
      case Left(failure) =>
        assert(failure.reason().get().getErrors.contains(ERROR_BUSINESS_VALIDATION_1))
        assert(failure.reason().get().getErrors.contains(ERROR_BUSINESS_VALIDATION_2))
      case _ => fail()
    }
  }

  it should "fail business processing" in {
    val command = TestCommand()
    class TestCommandHandler extends CommandHandler[TestCommand, TestFailureEvent, TestSuccessEvent] {
      override def technicalValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE)

      override def domainValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE)

      override def domainProcessing(command: TestCommand): DomainProcessingFunction[TestFailureEvent, TestSuccessEvent] =
        PROCESSING_BUSINESS_NEGATIVE
    }

    val commandHandler = new TestCommandHandler()
    val result = commandProcessor.execute[TestCommand,
      TestCommandHandler,
      TestFailureEvent,
      TestSuccessEvent](command, commandHandler, Optional.empty())
    result match {
      case Left(failure) =>
        assert(failure.reason().get().getErrors.contains(ERROR_BUSSINESS_PROCESSING))
      case _ => fail()
    }
  }

  it should "process everything successfully" in {
    val command = TestCommand()
    class TestCommandHandler extends CommandHandler[TestCommand, TestFailureEvent, TestSuccessEvent] {
      override def technicalValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE, VALIDATION_POSITIVE)

      override def domainValidationRules(command: TestCommand): util.List[ValidationRule] =
        util.Arrays.asList(VALIDATION_POSITIVE, VALIDATION_POSITIVE)

      override def domainProcessing(command: TestCommand): DomainProcessingFunction[TestFailureEvent, TestSuccessEvent] =
        () => Right(testSuccessEvent)
    }

    val commandHandler = new TestCommandHandler()
    val result = commandProcessor.execute[TestCommand,
      TestCommandHandler,
      TestFailureEvent,
      TestSuccessEvent](command, commandHandler, Optional.empty())
    result match {
      case Right(success) =>
        assert(success == testSuccessEvent)
      case _ => fail()
    }

  }
}

