/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs

import cloud.yantra.patterns.cqrs.errors.DomainError
import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec

class CommandProcessorTest extends FlatSpec with MockFactory {

  val commandProcessor = new CommandProcessor {
    override def commandRepository: CommandRepository = stub[CommandRepository]
  }

  case class TestCommand() extends Command {
    override val command: String = this.getClass.getName

    override val id: CommandId = CommandUuid()
  }

  val TECHNICAL_VALIDATION_ERROR1 = new DomainError {
    override def code: String = "DETE1"

    override def message: String = "Domain technical validation error 1"

    override def localizationMessageParameters: Seq[String] = Nil
  }

  val TECHNICAL_VALIDATION_ERROR2 = new DomainError {
    override def code: String = "DETE2"

    override def message: String = "Domain technical validation error 2"

    override def localizationMessageParameters: Seq[String] = Nil
  }


  val BUSINESS_VALIDATION_ERROR1 = new DomainError {
    override def code: String = "BTE1"

    override def message: String = "Business domain validation error 1"

    override def localizationMessageParameters: Seq[String] = Nil
  }
  val BUSINESS_VALIDATION_ERROR2 = new DomainError {
    override def code: String = "BTE2"

    override def message: String = "Business domain validation error 2"

    override def localizationMessageParameters: Seq[String] = Nil
  }

  val BUSSINESS_PROCESSING_ERROR = new DomainError {
    override def code: String = "BPE"

    override def message: String = "Business processing error"

    override def localizationMessageParameters: Seq[String] = Nil
  }

  val POSITIVE_VALIDATION: ValidationRule = () => Passed
  val NEGATIVE_TECHNICAL_VALIDATION1: ValidationRule = () => Result(TECHNICAL_VALIDATION_ERROR1)
  val NEGATIVE_TECHNICAL_VALIDATION2: ValidationRule = () => Result(TECHNICAL_VALIDATION_ERROR2)
  val NEGATIVE_BUSINESS_VALIDATION1: ValidationRule = () => Result(BUSINESS_VALIDATION_ERROR1)
  val NEGATIVE_BUSINESS_VALIDATION2: ValidationRule = () => Result(BUSINESS_VALIDATION_ERROR2)

  "Command processor" should "fail Technical validation" in {

    val command = TestCommand()
    val commandHandler = new CommandHandler[TestCommand] {
      override def technicalValidations(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION, NEGATIVE_TECHNICAL_VALIDATION1)

      override def businessValidation(command: TestCommand): Seq[ValidationRule] = Seq(NEGATIVE_BUSINESS_VALIDATION1)

      override def domainProcessing(command: TestCommand): AggregateProcessor = () => Passed
    }

    val result = commandProcessor.process(command, commandHandler)
    result match {
      case f: Failed => assert(f.errors.contains(TECHNICAL_VALIDATION_ERROR1))
      case _ => fail()
    }

  }

  it should "fail Technical validation on 1st encounter" in {

    val command = TestCommand()
    val commandHandler = new CommandHandler[TestCommand] {
      override def technicalValidations(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION, NEGATIVE_TECHNICAL_VALIDATION2, NEGATIVE_TECHNICAL_VALIDATION1)

      override def businessValidation(command: TestCommand): Seq[ValidationRule] = Seq(NEGATIVE_BUSINESS_VALIDATION1)

      override def domainProcessing(command: TestCommand): AggregateProcessor = () => Passed
    }

    val result = commandProcessor.process(command, commandHandler)
    result match {
      case f: Failed =>
        assert(f.errors.contains(TECHNICAL_VALIDATION_ERROR2))
        assert(!f.errors.contains(TECHNICAL_VALIDATION_ERROR1))
      case _ => fail()
    }

  }

  it should "fail business validation" in {
    val command = TestCommand()
    val commandHandler = new CommandHandler[TestCommand] {
      override def technicalValidations(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION)

      override def businessValidation(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION, POSITIVE_VALIDATION, NEGATIVE_BUSINESS_VALIDATION1)

      override def domainProcessing(command: TestCommand): AggregateProcessor = () => Passed
    }

    val result = commandProcessor.process(command, commandHandler)
    result match {
      case f: Failed => assert(f.errors.contains(BUSINESS_VALIDATION_ERROR1))
      case _ => fail()
    }
  }

  it should "fail business validation on 1st encounter" in {
    val command = TestCommand()
    val commandHandler = new CommandHandler[TestCommand] {
      override def technicalValidations(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION)

      override def businessValidation(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION, NEGATIVE_BUSINESS_VALIDATION2, NEGATIVE_BUSINESS_VALIDATION1)

      override def domainProcessing(command: TestCommand): AggregateProcessor = () => Passed
    }

    val result = commandProcessor.process(command, commandHandler)
    result match {
      case f: Failed =>
        assert(f.errors.contains(BUSINESS_VALIDATION_ERROR2))
        assert(!f.errors.contains(BUSINESS_VALIDATION_ERROR1))
      case _ => fail()
    }
  }

  it should "fail business processing" in {

    val command = TestCommand()
    val commandHandler = new CommandHandler[TestCommand] {
      override def technicalValidations(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION)

      override def businessValidation(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION)

      override def domainProcessing(command: TestCommand): AggregateProcessor = () => Result(BUSSINESS_PROCESSING_ERROR)
    }

    val result = commandProcessor.process(command, commandHandler)
    result match {
      case f: Failed =>
        assert(f.errors.contains(BUSSINESS_PROCESSING_ERROR))
      case _ => fail()
    }
  }

  it should "process everything successfully" in {

    val command = TestCommand()
    val commandHandler = new CommandHandler[TestCommand] {
      override def technicalValidations(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION, POSITIVE_VALIDATION)

      override def businessValidation(command: TestCommand): Seq[ValidationRule] = Seq(POSITIVE_VALIDATION, POSITIVE_VALIDATION)

      override def domainProcessing(command: TestCommand): AggregateProcessor = () => Passed
    }

    val result = commandProcessor.process(command, commandHandler)
    result match {
      case Passed => succeed
      case _ => fail()
    }
  }

}
