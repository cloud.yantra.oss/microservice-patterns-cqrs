/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs.errors

import org.scalatest.FunSuite

class DomainErrorTest extends FunSuite {

  case class DE(code: String,
                message: String,
                localizationMessageParameters: Seq[String]) extends DomainError

  val error1 = DE("DE", "Some message", Nil)
  val error2 = DE("DE", "Some message", Nil)

  test("Id is unique for every domain error") {
    assert(error1.id != error2.id)
  }

  test("test timestamp are different for every errors") {
    assert(error1.timestamp != error2.timestamp)
  }

  test("test detail is same as message when detail is not provided") {
    assert(error1.message == error1.detail)
  }

  test(" formatted message is same as message when localization information is absent") {
    assert(error1.message == error1.getFormattedMessage())
  }

  test("message is formatted properly when localization message is provided") {
    val localizableMessage = "Some message with value [{0}], [{1}] and [{2}]"
    val values = Seq("value1", "value2", "value3")
    val error3 = DE("DE", localizableMessage, values)
    assert(error3.message == localizableMessage)
    assert(error3.getFormattedMessage() == "Some message with value [value1], [value2] and [value3]")
  }

  test("values of default domain error should be UNSUPPORTED_REQUEST") {
    val domainError = DomainError.apply
    assert(domainError.code == "UNSUPPORTED_REQUEST")
    assert(domainError.message == "UNSUPPORTED_REQUEST")
    assert(domainError.localizationMessageParameters == Nil)
  }
}
