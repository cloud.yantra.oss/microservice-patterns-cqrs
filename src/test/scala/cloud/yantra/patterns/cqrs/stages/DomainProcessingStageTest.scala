/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.cqrs.stages

import org.scalatest.FunSuite

class DomainProcessingStageTest extends FunSuite {

  test("default stage for any processing stage is Received") {
    assert(DomainProcessingStage.apply == DomainProcessingStage.Received)
  }

  test("always return Received even if invalid stage is provided for domain processing stage") {
    assert(DomainProcessingStage.apply("Some Value") == DomainProcessingStage.Received)
  }

  test("returns technical validation when it is sought") {
    assert(DomainProcessingStage.apply("technical_validation") == DomainProcessingStage.TechnicalValidation)
  }

  test("returns business validation when it is sought") {
    assert(DomainProcessingStage.apply("business_validation") == DomainProcessingStage.BusinessValidation)
  }

  test("returns aggregate mutation when it is sought") {
    assert(DomainProcessingStage.apply("aggregate_mutation") == DomainProcessingStage.AggregateMutation)
  }

  test("testUnapply") {
    assert(DomainProcessingStage.unapply(DomainProcessingStage.Received) == Some("RECEIVED"))
    assert(DomainProcessingStage.unapply(DomainProcessingStage.TechnicalValidation) == Some("TECHNICAL_VALIDATION"))
    assert(DomainProcessingStage.unapply(DomainProcessingStage.BusinessValidation) == Some("BUSINESS_VALIDATION"))
    assert(DomainProcessingStage.unapply(DomainProcessingStage.AggregateMutation) == Some("AGGREGATE_MUTATION"))
  }
}
