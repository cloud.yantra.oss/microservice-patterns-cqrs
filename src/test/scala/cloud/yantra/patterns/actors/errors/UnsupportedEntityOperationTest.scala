/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.errors

import cloud.yantra.patterns.es.{Entity, EntityId, EntityUuid}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

class UnsupportedEntityOperationTest extends FlatSpecLike
  with Matchers
  with BeforeAndAfterAll {

  val entity = new Entity {
    override val id: EntityId = EntityUuid()

    override def entity: String = "TestEntity"
  }
  val error = UnsupportedEntityOperation(entity.getClass, Some(entity.id), "Some operation")

  "UnsupportedEntityOperation" must
    "contain valid code" in {
    assert(error.code == classOf[UnsupportedEntityOperation].getSimpleName)
  }

  it must "contain all values for localization" in {
    assert(error.localizationMessageParameters.contains(entity.id.value))
    assert(error.localizationMessageParameters.contains(entity.getClass.getSimpleName))
    assert(error.localizationMessageParameters.contains(error.operation.getClass.getSimpleName))
  }

  it must "return entity id as 'undefined' when not entity id is present" in {
    val errorWithNoEntityId = UnsupportedEntityOperation(entity.getClass, None, "Some operation")
    assert(errorWithNoEntityId.localizationMessageParameters.contains("undefined"))
  }
}
