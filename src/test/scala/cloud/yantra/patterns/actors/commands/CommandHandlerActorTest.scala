/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.commands

import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, PoisonPill, Props}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Subscribe
import akka.testkit.{TestKit, TestProbe}
import cloud.yantra.patterns.actors.FrameworkActorTest
import cloud.yantra.patterns.actors.commands.CommandHandlerActor.Events._
import cloud.yantra.patterns.actors.entities.EntityManagerActor.Commands.AuthorizedCommand
import cloud.yantra.patterns.actors.errors.UnsupportedOperation
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.errors.DomainError
import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage
import cloud.yantra.patterns.es._

import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.language.postfixOps

class CommandHandlerActorTest extends TestKit(ActorSystem(classOf[CommandHandlerActorTest].getSimpleName))
  with FrameworkActorTest {

  private lazy val TEST_ENTITY_MANAGER_VALIDATED_COMMAND_TOPIC = s"TEST_ENTITY_MANAGER_VALIDATED_COMMAND_TOPIC_${UUID.randomUUID()}"

  case class TestCommand(id: CommandId = CommandUuid(),
                         property: String = "Some test property") extends Command

  class TestCommandHandler(commandId: CommandId,
                           techRules: Seq[Rule],
                           bizRules: Seq[Rule]) extends CommandHandlerActor[TestCommand](classOf[TestCommand],
    commandId) {

    override def technicalDomainCommandValidationRules(cmd: TestCommand): Seq[Rule] = techRules

    override def businessDomainCommandValidationRules(cmd: TestCommand): Seq[Rule] = bizRules

    override lazy val entityManagerToValidatedCommandTopic: String = TEST_ENTITY_MANAGER_VALIDATED_COMMAND_TOPIC
  }

  object TestCommandHandler {
    def props(commandId: CommandId, techRules: Seq[Rule], bizRules: Seq[Rule]): Props =
      Props(new TestCommandHandler(commandId, techRules, bizRules))
  }

  case class TestRule(result: Result) extends Rule {
    override val id: RuleId = RuleIdUuid()
    override val validate: ValidationRule = () => result
  }

  case class LongTestRule(duration: Duration) extends Rule {
    override val id: RuleId = RuleIdUuid()
    override val validate: ValidationRule = () => {
      Thread.sleep(duration.toMillis)
      Passed
    }
  }

  case object TestError1 extends DomainError {
    override val code: String = "TestError1"

    override val message: String = "Domain technical validation error 1"

    override val localizationMessageParameters: Seq[String] = Nil
  }

  private lazy val allPassingRules = Seq(TestRule(Passed),
    TestRule(Passed),
    TestRule(Passed),
    TestRule(Passed),
    TestRule(Passed),
    TestRule(Passed))

  private lazy val someFailingRules = Seq(TestRule(Passed),
    TestRule(Result(TestError1)),
    TestRule(Passed))

  "Command handler actor" must
    "accept supported command and respond with command received event" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, allPassingRules, allPassingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
  }

  it must "reject commands not recognized by it" in {
    val bot = TestProbe("CommandShooter")
    val unsupportedCmd = new Command {
      override val id: CommandId = CommandUuid()
    }
    val handler = bot.childActorOf(TestCommandHandler.props(unsupportedCmd.id, allPassingRules, allPassingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(unsupportedCmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandProcessingFailed(id, stage, result) =>
        assert(id == unsupportedCmd.id)
        assert(stage == DomainProcessingStage.Received)
        assert(result.errors.head.code == classOf[UnsupportedOperation].getSimpleName)
    }
  }

  it must "accept only ONE command, and reject additional commands send" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand(property = "First Command")
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id,
      Seq(LongTestRule(FiniteDuration(10, TimeUnit.SECONDS))),
      allPassingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    val cmdAnother = TestCommand(property = "Another command")
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmdAnother, Some(bot.ref)))
    bot.expectNoMessage()
    bot.ref ! PoisonPill
  }

  it must "respond with technical validation success event, when technical validation passed" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, allPassingRules, someFailingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case TechnicalValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
  }

  it must "respond with technical validation success event, when technical validation rule set is empty" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, Nil, someFailingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case TechnicalValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
  }

  it must "respond with command processing failed event, when technical validation fails" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, someFailingRules, allPassingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case CommandProcessingFailed(id, stage, _) =>
        assert(id == cmd.id)
        assert(stage == DomainProcessingStage.TechnicalValidation)
    }
  }

  it must "respond with business validation success event, when business validation passed" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, allPassingRules, allPassingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case TechnicalValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case BusinessValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
  }

  it must "respond with business validation success event, when rule sets for both technical and business validation are empty" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, Nil, Nil))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case TechnicalValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case BusinessValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
  }

  it must "respond with command processing failed event, when business validation fails" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, allPassingRules, someFailingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case TechnicalValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case CommandProcessingFailed(id, stage, _) =>
        assert(id == cmd.id)
        assert(stage == DomainProcessingStage.BusinessValidation)
    }
  }

  it must "respond with command processing failed event, when aggregate mutation fails" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, allPassingRules, allPassingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case TechnicalValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case BusinessValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
    bot.send(handler, ZeroEvent(None, Nil))
    bot.expectMsgPF() {
      case CommandProcessingFailed(id, stage, _) =>
        assert(id == cmd.id)
        assert(stage == DomainProcessingStage.AggregateMutation)
    }
  }

  it must "respond with aggegate mutation success event, when handler has handled aggregate successfully" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val entitySuccessEvent = new Event {
      override val id: EventId = EventUuid()

      override val entityId: Option[EntityId] = Some(EntityUuid())
    }
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, allPassingRules, allPassingRules))
    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case TechnicalValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case BusinessValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
    bot.send(handler, entitySuccessEvent)
    bot.expectMsgPF() {
      case AggregateMutationSuccessful(id, entityId) =>
        assert(id == cmd.id)
        assert(entityId == entitySuccessEvent.entityId.get)
    }
  }

  it must "publish to authorized command topic, belonging to manager of entity" in {
    val bot = TestProbe("CommandShooter")
    val cmd = TestCommand()
    val entitySuccessEvent = new Event {
      override val id: EventId = EventUuid()

      override val entityId: Option[EntityId] = Some(EntityUuid())
    }
    val handler = bot.childActorOf(TestCommandHandler.props(cmd.id, allPassingRules, allPassingRules))
    DistributedPubSub(system).mediator ! Subscribe(TEST_ENTITY_MANAGER_VALIDATED_COMMAND_TOPIC, bot.ref)

    bot.send(handler, CommandHandlerActor.Commands.ProcessCommand(cmd, Some(bot.ref)))
    bot.expectMsgPF() {
      case CommandReceived(TestCommand(id, _)) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case TechnicalValidationSuccessful(id) =>
        assert(id == cmd.id)
    }
    bot.expectMsgPF() {
      case BusinessValidationSuccessful(id) =>
        assert(id == cmd.id)
    }

    bot.expectMsgPF() {
      case AuthorizedCommand(command, commandHandler) =>
        assert(command.id == cmd.id)
        assert(commandHandler.get == handler)
    }
  }
}
