/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.commands

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Publish, Subscribe}
import akka.testkit.{TestKit, TestProbe}
import cloud.yantra.patterns.actors.FrameworkActorTest
import cloud.yantra.patterns.actors.envelope.StationHeader
import cloud.yantra.patterns.actors.validations.CommandValidation.ValidationCompleted
import cloud.yantra.patterns.actors.validations.{BusinessDomainCommandValidation, TechnicalDomainCommandValidation}
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.errors.DomainError
import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage

import scala.concurrent.duration._
import scala.language.postfixOps

class CommandProcessingActorTest
  extends TestKit(ActorSystem.create(classOf[CommandProcessingActorTest].getSimpleName))
    with FrameworkActorTest {

  case class TestCommand(id: CommandId = CommandUuid(),
                         property: String = "Some test property", observer: ActorRef) extends Command with StationHeader {
    override def originator(): Option[Station] = None

    override def destination(): Seq[Station] = Nil

    override def intermediates(): Seq[Station] = Nil

    override def resultObservers(): Seq[Station] = Seq(observer)
  }

  case class TestRule(result: Result) extends Rule {
    override val id: RuleId = RuleIdUuid()
    override val validate: ValidationRule = () => result
  }

  case object TestError1 extends DomainError {
    override val code: String = "TestError1"

    override val message: String = "Domain technical validation error 1"

    override val localizationMessageParameters: Seq[String] = Nil
  }

  case class TechnicalValidation(commandId: CommandId,
                                 rules: Seq[Rule])
    extends TechnicalDomainCommandValidation[TestCommand](commandId)

  case class BusinessValidation(commandId: CommandId,
                                rules: Seq[Rule])
    extends BusinessDomainCommandValidation[TestCommand](commandId)

  case class TestCommandProcessorManagerActorOne() extends CommandProcessorManagerActor[TestCommand] {

    override def subscribeToEntityManagerEventTopics: Seq[String] = Nil

    override def subscribeToIncomingCommandTopics: Seq[String] = Nil

    override def publishToEntityManagerCommandTopics: Seq[String] = Seq(classOf[CommandProcessingActorTest].getSimpleName)

    override def technicalDomainValidationHandler(command: TestCommand): Props = {
      Props(
        new TechnicalValidation(command.id, Seq(TestRule(Passed),
          TestRule(Passed),
          TestRule(Passed),
          TestRule(Passed),
          TestRule(Passed),
          TestRule(Passed)))
      )
    }

    override def businessDomainValidationHandler(command: TestCommand): Props = {
      Props(
        new BusinessValidation(command.id, Seq(TestRule(Passed),
          TestRule(Passed),
          TestRule(Passed),
          TestRule(Passed),
          TestRule(Passed),
          TestRule(Passed)))
      )
    }
  }

  case class TestCommandProcessorManagerActorTwo() extends CommandProcessorManagerActor[TestCommand] {

    override def subscribeToEntityManagerEventTopics: Seq[String] = Nil

    override def subscribeToIncomingCommandTopics: Seq[String] = Nil

    override def publishToEntityManagerCommandTopics: Seq[String] = Seq(classOf[CommandProcessingActorTest].getSimpleName)

    override def technicalDomainValidationHandler(command: TestCommand): Props =
      Props(new TechnicalValidation(command.id, Seq(TestRule(Result(TestError1)))))

    override def businessDomainValidationHandler(command: TestCommand): Props =
      Props(new BusinessValidation(command.id, Seq(TestRule(Passed))))

  }

  case class TestCommandProcessorManagerActorThree() extends CommandProcessorManagerActor[TestCommand] {

    override def subscribeToEntityManagerEventTopics: Seq[String] = Nil

    override def subscribeToIncomingCommandTopics: Seq[String] = Nil

    override def publishToEntityManagerCommandTopics: Seq[String] = Seq(classOf[CommandProcessingActorTest].getSimpleName)

    override def technicalDomainValidationHandler(command: TestCommand): Props =
      Props(new TechnicalValidation(command.id, Seq(TestRule(Result(TestError1)))))

    override def businessDomainValidationHandler(command: TestCommand): Props =
      Props(new BusinessValidation(command.id, Seq(TestRule(Result(TestError1)))))

  }

  case class TestCommandProcessorManagerActorFour() extends CommandProcessorManagerActor[TestCommand] {

    override def subscribeToEntityManagerEventTopics: Seq[String] = Nil

    override def subscribeToIncomingCommandTopics: Seq[String] = Nil

    override def publishToEntityManagerCommandTopics: Seq[String] = Seq(classOf[CommandProcessingActorTest].getSimpleName)

    override def technicalDomainValidationHandler(command: TestCommand): Props =
      Props(new TechnicalValidation(command.id, Seq(TestRule(Passed))))

    override def businessDomainValidationHandler(command: TestCommand): Props =
      Props(new BusinessValidation(command.id, Seq(TestRule(Result(TestError1)))))

  }

  case class TestCommandProcessorActorFive() extends CommandProcessorManagerActor[TestCommand] {
    // This one listens to incoming command topics
    override def subscribeToEntityManagerEventTopics: Seq[String] = Nil

    override def subscribeToIncomingCommandTopics: Seq[String] = Seq(classOf[TestCommandProcessorActorFive].getSimpleName)

    override def publishToEntityManagerCommandTopics: Seq[String] = Seq(classOf[CommandProcessingActorTest].getSimpleName)

    override def technicalDomainValidationHandler(command: TestCommand): Props =
      Props(new TechnicalValidation(command.id, Seq(TestRule(Passed))))

    override def businessDomainValidationHandler(command: TestCommand): Props =
      Props(new BusinessValidation(command.id, Seq(TestRule(Passed))))

  }

  "CommandProcessingActor" must
    "report failure during validation" in {
    val commandId: CommandId = CommandUuid()
    val bot = TestProbe("CommandProcessingBot")
    val cmd = TestCommand(commandId, observer = bot.ref)
    val mediator = DistributedPubSub(system).mediator
    mediator ! Subscribe(classOf[CommandProcessingActorTest].getSimpleName, bot.ref)
    val cmdProcessor = bot.childActorOf(Props(new TestCommandProcessorManagerActorTwo()))
    bot.send(cmdProcessor, cmd)
    within(5000 millis) {
      expectNoMessage
      bot.expectMsgPF() {
        case ValidationCompleted(_, DomainProcessingStage.TechnicalValidation, _, failed: Failed) =>
          assert(failed.errors.head == TestError1)
      }
    }
  }

  it must "fail at technical validation stage when both technical and business validation also have error" in {
    val commandId: CommandId = CommandUuid()
    val bot = TestProbe("CommandProcessingBot")
    val cmd = TestCommand(commandId, observer = bot.ref)
    val mediator = DistributedPubSub(system).mediator
    mediator ! Subscribe(classOf[CommandProcessingActorTest].getSimpleName, bot.ref)
    val cmdProcessor = bot.childActorOf(Props(new TestCommandProcessorManagerActorThree()))
    bot.send(cmdProcessor, cmd)
    within(5000 millis) {
      expectNoMessage
      bot.expectMsgPF() {
        case ValidationCompleted(_, DomainProcessingStage.TechnicalValidation, _, failed: Failed) =>
          assert(failed.errors.head == TestError1)
      }
    }
  }

  it must "fail at business validation stage when business validation has error, but technical stage is passed" in {
    val commandId: CommandId = CommandUuid()
    val bot = TestProbe("CommandProcessingBot")
    val cmd = TestCommand(commandId, observer = bot.ref)
    val mediator = DistributedPubSub(system).mediator
    mediator ! Subscribe(classOf[CommandProcessingActorTest].getSimpleName, bot.ref)
    val cmdProcessor = bot.childActorOf(Props(new TestCommandProcessorManagerActorFour()))
    bot.send(cmdProcessor, cmd)
    within(5000 millis) {
      expectNoMessage
      bot.expectMsgPF() {
        case ValidationCompleted(_, DomainProcessingStage.BusinessValidation, _, failed: Failed) =>
          assert(failed.errors.head == TestError1)
      }
    }
  }

  it must "domain validation, after successful completion of technical and business validations" in {
    val commandId: CommandId = CommandUuid()
    val bot = TestProbe("CommandProcessingBot")
    val cmd = TestCommand(commandId, observer = bot.ref)
    val mediator = DistributedPubSub(system).mediator
    mediator ! Subscribe(classOf[CommandProcessingActorTest].getSimpleName, bot.ref)
    val cmdProcessor = bot.childActorOf(Props(new TestCommandProcessorManagerActorOne()))
    bot.send(cmdProcessor, cmd)
    within(1 second) {
      expectNoMessage
      bot.expectMsgClass(cmd.getClass)
    }
  }

  it must "process command, when published to incoming command topics it has subscribed to" in {
    val commandId: CommandId = CommandUuid()
    val bot = TestProbe("CommandProcessingBot")
    val cmd = TestCommand(commandId, "Special command  ====== published to a topic", observer = bot.ref)
    val cmdProcessor = bot.childActorOf(Props(new TestCommandProcessorActorFive()))
    val mediator = DistributedPubSub(system).mediator
    mediator ! Subscribe(classOf[CommandProcessingActorTest].getSimpleName, bot.ref)

    within(1 second) {
      expectNoMessage
      mediator ! Publish(classOf[TestCommandProcessorActorFive].getSimpleName, cmd)
      expectNoMessage
      bot.expectMsgPF(5 seconds, "Receiving validated command.") {
        case TestCommand(id, _, _) =>
          assert(id == commandId)
        case anything =>
          fail(s"Received unexpected message of type [${anything.getClass}] holding data [${anything}]")
      }
    }
  }

  it must "listen to Events published by entity manager, post sending message as part of domain processing " in {
    pending
  }

}
