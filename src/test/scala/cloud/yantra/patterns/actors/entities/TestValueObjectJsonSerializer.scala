/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.entities

import java.io.NotSerializableException

import akka.serialization.SerializerWithStringManifest
import cloud.yantra.patterns.actors.entities.TestValueObjectPersistentActor.Events._
import cloud.yantra.patterns.es.{EntityId, EventId}
import play.api.libs.json._

class TestValueObjectJsonSerializer extends SerializerWithStringManifest {

  implicit object entityIdReads extends Reads[EntityId] {
    override def reads(json: JsValue): JsResult[EntityId] = json.validate[String] match {
      case JsSuccess(v, _) => JsSuccess(EntityId(v))
      case unknown => JsError(s"Could not process ${unknown} for EntityId")
    }
  }

  implicit object entityIdWrites extends Writes[EntityId] {
    override def writes(id: EntityId): JsValue = Json.toJson(id.value)
  }


  implicit object eventIdReads extends Reads[EventId] {
    override def reads(json: JsValue): JsResult[EventId] = json.validate[String] match {
      case JsSuccess(v, _) => JsSuccess(EventId(v))
      case unknown => JsError(s"Could not process ${unknown} for EventId")
    }
  }

  implicit object eventIdWrites extends Writes[EventId] {
    override def writes(id: EventId): JsValue = Json.toJson(id.value)
  }


  implicit val testValueObjectCreatedReads = Json.reads[TestValueObjectCreated]
  implicit val testValueObjectCreatedWrites = Json.writes[TestValueObjectCreated]
  implicit val testValueObjectCountDecreasedReads = Json.reads[TestValueObjectCountDecreased]
  implicit val testValueObjectCountDecreasedWrites = Json.writes[TestValueObjectCountDecreased]
  implicit val testValueObjectCountIncreasedReads = Json.reads[TestValueObjectCountIncreased]
  implicit val testValueObjectCountIncreasedWrites = Json.writes[TestValueObjectCountIncreased]

  override val identifier: Int = 20181232

  override def manifest(o: AnyRef): String = o.getClass.getCanonicalName

  override def toBinary(o: AnyRef): Array[Byte] = o match {

    case e: TestValueObjectCreated => Json.toBytes(Json.toJson(e))
    case e: TestValueObjectCountIncreased => Json.toBytes(Json.toJson(e))
    case e: TestValueObjectCountDecreased => Json.toBytes(Json.toJson(e))
    case unknown =>
      throw new Exception(s"Cannot serialize [${unknown}] with [${this.getClass.getCanonicalName}].")
  }


  override def fromBinary(bytes: Array[Byte], manifest: String): AnyRef = {
    val json = Json.parse(bytes)
    manifest match {
      case "cloud.yantra.patterns.actors.entities.TestValueObjectPersistentActor.Events$.TestValueObjectCreated" =>
        json.as[TestValueObjectCreated]
      case "cloud.yantra.patterns.actors.entities.TestValueObjectPersistentActor.Events$.TestValueObjectCountIncreased" =>
        json.as[TestValueObjectCountIncreased]
      case "cloud.yantra.patterns.actors.entities.TestValueObjectPersistentActor.Events$.TestValueObjectCountDecreased" =>
        json.as[TestValueObjectCountDecreased]
      case anything =>
        throw new NotSerializableException(s"Serialization case absent for [${anything}]")
    }
  }

}
