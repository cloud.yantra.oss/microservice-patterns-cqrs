/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.entities

import cloud.yantra.patterns.es._

case class TestValueObject(override val id: EntityId,
                           count: Int = 0) extends ValueObject {

  override val entity: String = classOf[TestValueObject].getSimpleName

  def -(i: Int): TestValueObject = TestValueObject(id, count - i)

  def +(i: Int): TestValueObject = TestValueObject(id, count + i)

}
