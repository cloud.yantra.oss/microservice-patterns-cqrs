/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.entities

import akka.actor.Props
import cloud.yantra.patterns.actors.entities.TestValueObjectPersistentActor.Commands.{Create, DecrementCount, IncrementCount}
import cloud.yantra.patterns.actors.entities.TestValueObjectPersistentActor.Events.{TestValueObjectCountDecreased, TestValueObjectCountIncreased, TestValueObjectCreated}
import cloud.yantra.patterns.cqrs.{Command, CommandId, CreateCommand, UpdateCommand}
import cloud.yantra.patterns.es._

import scala.collection.immutable.HashMap

class TestValueObjectPersistentActor(entityId: EntityId)
  extends EntityPersistentActor[TestValueObject](classOf[TestValueObject],
    entityId) {

  override def entityInitializationHandler(command: CreateCommand[TestValueObject]): Event = command match {
    case c: Create =>
      TestValueObjectCreated(EventId(), entityId, c.seedCount)
    case _ =>
      ZeroEvent(Some(entityId), Nil)
  }

  override def commandHandlers: Map[Class[_ <: UpdateCommand[TestValueObject]], (TestValueObject, UpdateCommand[TestValueObject]) => Event] = HashMap(
    (classOf[IncrementCount], handleIncrementCount _),
    (classOf[DecrementCount], handleDecrementCount _)
  )

  override def eventHandlers: Map[Class[_ <: Event], (Option[TestValueObject], Event) => Option[TestValueObject]] = HashMap(
    (classOf[TestValueObjectCreated], update _),
    (classOf[TestValueObjectCountIncreased], update _),
    (classOf[TestValueObjectCountDecreased], update _),
    (classOf[ZeroEvent], update _)
  )

  def handleIncrementCount(entity: TestValueObject, cmd: Command): Event = cmd match {
    case c: IncrementCount if c.entityId == entity.id =>
      TestValueObjectCountIncreased(EventId(), entityId, c.incrementBy)
    case _ =>
      ZeroEvent(Some(entityId), Nil)
  }

  def handleDecrementCount(entity: TestValueObject, cmd: Command): Event = cmd match {
    case c: DecrementCount if c.entityId == entity.id =>
      TestValueObjectCountDecreased(EventId(), entityId, c.decrementBy)
    case _ =>
      ZeroEvent(Some(entityId), Nil)
  }

  def update(entity: Option[TestValueObject], event: Event): Option[TestValueObject] = event match {
    case TestValueObjectCreated(_, entityId, cnt) => Some(TestValueObject(entityId, cnt))
    case TestValueObjectCountIncreased(_, _, increment) => Some(entity.get + increment)
    case TestValueObjectCountDecreased(_, _, decrement) => Some(entity.get - decrement)
    case _ => entity
  }
}

object TestValueObjectPersistentActor {

  def props(entityId: EntityId): Props = Props(new TestValueObjectPersistentActor(entityId))

  object Commands {

    case class Create(override val id: CommandId, seedCount: Int) extends CreateCommand[TestValueObject]

    case class IncrementCount(override val id: CommandId,
                              override val entityId: EntityId,
                              incrementBy: Int) extends UpdateCommand[TestValueObject]

    case class DecrementCount(override val id: CommandId,
                              override val entityId: EntityId,
                              decrementBy: Int) extends UpdateCommand[TestValueObject]

  }

  object Events {


    case class TestValueObjectCreated(override val id: EventId = EventId(),
                                      entityIdentity: EntityId,
                                      count: Int = 0) extends Event
      with Publishable {
      override def entityId: Option[EntityId] = Some(entityIdentity)

      override def topics(): Seq[String] = Seq(this.getClass.getCanonicalName)
    }

    case class TestValueObjectCountIncreased(override val id: EventId = EventId(),
                                             entityIdentity: EntityId,
                                             countIncreasedBy: Int
                                            ) extends Event
      with Publishable {
      override def entityId: Option[EntityId] = Some(entityIdentity)

      override def topics(): Seq[String] = Seq(classOf[TestValueObject].getSimpleName)
    }

    case class TestValueObjectCountDecreased(override val id: EventId = EventId(),
                                             entityIdentity: EntityId,
                                             countDecreasedBy: Int
                                            ) extends Event
      with Publishable {
      override def entityId: Option[EntityId] = Some(entityIdentity)

      override def topics(): Seq[String] = Seq(classOf[TestValueObject].getSimpleName)
    }

  }

}
