/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.entities

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import cloud.yantra.patterns.actors.FrameworkActorTest
import cloud.yantra.patterns.actors.entities.EntityManagerActor.Commands.AuthorizedCommand
import cloud.yantra.patterns.actors.entities.EntityManagerActor.Events.ObservedEvent
import cloud.yantra.patterns.actors.entities.EntityPersistentActor.Commands.GetEntity
import cloud.yantra.patterns.actors.entities.TestValueObjectPersistentActor.Events.TestValueObjectCreated
import cloud.yantra.patterns.cqrs.CommandId
import cloud.yantra.patterns.es.{EntityUuid, ZeroEvent}

import scala.concurrent.duration._
import scala.language.postfixOps

class EntityPersistentActorTest
  extends TestKit(ActorSystem.create(classOf[EntityPersistentActorTest].getSimpleName))
    with FrameworkActorTest {

  "The EntityActor" must
    "create an instance of entity" in {
    val cmdId = CommandId()
    val entity = TestValueObject(EntityUuid(), 10)
    val cmdCreate = TestValueObjectPersistentActor.Commands.Create(cmdId, 10)
    val parent = TestProbe()
    val entityActor = parent.childActorOf(TestValueObjectPersistentActor.props(entity.id))

    parent.send(entityActor, AuthorizedCommand(cmdCreate, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCreated(_, _, cnt), Some(ref)) =>
        assert(cnt == 10)
        assert(ref == parent.ref)
    }

    parent.send(entityActor, AuthorizedCommand(GetEntity(CommandId(), entity.id, parent.ref), None))
    parent.expectMsg(TestValueObject(entity.id, 10))
  }

  it must "respond with ZeroEvent when unrecognized command is sent instead of creation" in {
    val entity = TestValueObject(EntityUuid(), 10)
    val parent = TestProbe()
    val entityActor = parent.childActorOf(TestValueObjectPersistentActor.props(entity.id))
    parent.send(entityActor, "A DUMMY COMMAND")
    parent.expectMsgPF(10 seconds) {
      case ZeroEvent(_, _) =>
        assert(true)
      case unexpected =>
        fail(s"Received unexpected response as [${unexpected}]")
    }
  }

  it must "not allow re-creation of an entity once created" in {

    val cmdId = CommandId()
    val entity = TestValueObject(EntityUuid(), 10)
    val cmdCreate = TestValueObjectPersistentActor.Commands.Create(cmdId, 10)
    val parent = TestProbe()
    val entityActor = parent.childActorOf(TestValueObjectPersistentActor.props(entity.id))

    parent.send(entityActor, AuthorizedCommand(cmdCreate, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCreated(_, _, cnt), Some(ref)) =>
        assert(cnt == 10)
        assert(ref == parent.ref)
    }

    parent.send(entityActor, AuthorizedCommand(GetEntity(CommandId(), entity.id, parent.ref), None))
    parent.expectMsg(TestValueObject(entity.id, 10))

    parent.send(entityActor, AuthorizedCommand(cmdCreate, Some(parent.ref)))
    parent.expectMsgPF() {
      case ZeroEvent(_, _) =>
        assert(true)
      case unexpected =>
        fail(s"Received unexpected response as [${unexpected}]")
    }
  }

  it must "respond with ZeroEvent when unrecognized command is sent instead of update commands, when in updated mode" in {
    val cmdId = CommandId()
    val entity = TestValueObject(EntityUuid(), 10)
    val cmdCreate = TestValueObjectPersistentActor.Commands.Create(cmdId, 10)
    val parent = TestProbe()
    val entityActor = parent.childActorOf(TestValueObjectPersistentActor.props(entity.id))

    parent.send(entityActor, AuthorizedCommand(cmdCreate, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCreated(_, _, cnt), Some(ref)) =>
        assert(cnt == 10)
        assert(ref == parent.ref)
    }
    
    parent.send(entityActor, "A DUMMY COMMAND")
    parent.expectMsgPF(10 seconds) {
      case ZeroEvent(_, _) =>
        assert(true)
      case unexpected =>
        fail(s"Received unexpected response as [${unexpected}]")
    }
  }

  it must "increment count by 5 when asked to increment by 5" in {
    import TestValueObjectPersistentActor.Commands._
    import TestValueObjectPersistentActor.Events._
    val cmdId = CommandId()
    val entity = TestValueObject(EntityUuid(), 10)
    val cmdCreate = Create(cmdId, 10)
    val cmdIncrement = IncrementCount(CommandId(), entity.id, 5)

    val parent = TestProbe()
    val entityActor = parent.childActorOf(TestValueObjectPersistentActor.props(entity.id))
    parent.send(entityActor, AuthorizedCommand(cmdCreate, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCreated(_, _, cnt), Some(ref)) =>
        assert(cnt == 10)
        assert(ref == parent.ref)
    }

    parent.send(entityActor, AuthorizedCommand(cmdIncrement, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCountIncreased(_, entityIdentity, incrementCount), Some(ref)) =>
        assert(entityIdentity == entity.id)
        assert(incrementCount == 5)
        assert(ref == parent.ref)
    }

    parent.send(entityActor, AuthorizedCommand(GetEntity(CommandId(), entity.id, parent.ref), None))
    parent.expectMsg(entity + 5)
  }

  it must "decrease count by 5 when asked to decrease by 5" in {
    import TestValueObjectPersistentActor.Commands._
    import TestValueObjectPersistentActor.Events._
    val cmdId = CommandId()
    val entity = TestValueObject(EntityUuid(), 10)
    val cmdCreate = Create(cmdId, 10)
    val cmdDecrement = DecrementCount(CommandId(), entity.id, 5)

    val parent = TestProbe()
    val entityActor = parent.childActorOf(TestValueObjectPersistentActor.props(entity.id))
    parent.send(entityActor, AuthorizedCommand(cmdCreate, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCreated(_, entityIdentity, count), Some(ref)) =>
        assert(entityIdentity == entity.id)
        assert(count == entity.count)
        assert(ref == parent.ref)
    }

    parent.send(entityActor, AuthorizedCommand(cmdDecrement, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCountDecreased(_, entityIdentity, decrementCount), _) =>
        assert(entityIdentity == entity.id)
        assert(decrementCount == 5)
    }

    parent.send(entityActor, AuthorizedCommand(GetEntity(CommandId(), entity.id, parent.ref), None))
    parent.expectMsg(entity - 5)
  }

  it must "reach its original state when increased and decreased by same count " in {
    import TestValueObjectPersistentActor.Commands._
    import TestValueObjectPersistentActor.Events._
    val cmdId = CommandId()
    val entity = TestValueObject(EntityUuid(), 10)
    val cmdCreate = Create(cmdId, 10)
    val cmdIncrement = IncrementCount(CommandId(), entity.id, 5)
    val cmdDecrement = DecrementCount(CommandId(), entity.id, 5)

    val parent = TestProbe()
    val entityActor = parent.childActorOf(TestValueObjectPersistentActor.props(entity.id))
    parent.send(entityActor, AuthorizedCommand(cmdCreate, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCreated(_, _, cnt), Some(ref)) =>
        assert(cnt == 10)
        assert(ref == parent.ref)
    }

    parent.send(entityActor, AuthorizedCommand(cmdDecrement, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCountDecreased(_, entityIdentity, decrementCount), Some(ref)) =>
        assert(entityIdentity == entity.id)
        assert(decrementCount == 5)
        assert(ref == parent.ref)
    }

    parent.send(entityActor, AuthorizedCommand(cmdIncrement, Some(parent.ref)))
    parent.expectMsgPF() {
      case ObservedEvent(TestValueObjectCountIncreased(_, entityIdentity, incrementCount), Some(ref)) =>
        assert(entityIdentity == entity.id)
        assert(incrementCount == 5)
        assert(ref == parent.ref)
    }

    parent.send(entityActor, AuthorizedCommand(GetEntity(CommandId(), entity.id, parent.ref), None))
    parent.expectMsg(entity)
  }
}
