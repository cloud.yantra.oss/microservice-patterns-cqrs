/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.entities

import akka.actor.{ActorSystem, PoisonPill}
import akka.testkit.{TestKit, TestProbe}
import cloud.yantra.patterns.actors.FrameworkActorTest
import cloud.yantra.patterns.actors.entities.EntityManagerActor.Commands.AuthorizedCommand
import cloud.yantra.patterns.actors.entities.EntityManagerActor.Events.ObservedEvent
import cloud.yantra.patterns.actors.entities.EntityPersistentActor.Commands.GetEntity
import cloud.yantra.patterns.cqrs.CommandId
import cloud.yantra.patterns.es.EntityUuid

import scala.concurrent.duration._
import scala.language.postfixOps

class EntityPersistentActorRecoveryTest
  extends TestKit(ActorSystem.create(classOf[EntityPersistentActorRecoveryTest].getSimpleName))
    with FrameworkActorTest {

  "The persistent entity actor" must "recover post stopping" in {
    import TestValueObjectPersistentActor.Commands._
    import TestValueObjectPersistentActor.Events._
    val parent1 = TestProbe("NewEntity")
    val cmdId = CommandId()
    val entity = TestValueObject(EntityUuid(), 10)
    val cmdGetEntityParent1 = GetEntity(CommandId(), entity.id, parent1.ref)
    val cmdCreate = Create(cmdId, 10)
    val cmdIncrement = IncrementCount(CommandId(), entity.id, 5)

    val cmdDecrement = DecrementCount(CommandId(), entity.id, 5)
    val newEntity = parent1.childActorOf(TestValueObjectPersistentActor.props(entity.id),
      EntityPersistentActor.entityPersistenceId(classOf[TestValueObject], entity.id))

    parent1.send(newEntity, AuthorizedCommand(cmdCreate, None))
    parent1.expectMsgPF() {
      case ObservedEvent(TestValueObjectCreated(_, entityIdentity, count), None) =>
        assert(entityIdentity == entity.id)
        assert(count == entity.count)
    }
    parent1.send(newEntity, AuthorizedCommand(cmdGetEntityParent1, None))
    parent1.expectMsg(entity)
    parent1.send(newEntity, AuthorizedCommand(cmdIncrement, None))
    parent1.expectMsgClass(classOf[ObservedEvent])
    parent1.send(newEntity, AuthorizedCommand(cmdIncrement, None))
    parent1.expectMsgClass(classOf[ObservedEvent])
    parent1.send(newEntity, AuthorizedCommand(cmdDecrement, None))
    parent1.expectMsgClass(classOf[ObservedEvent])
    parent1.send(newEntity, EntityPersistentActor.Commands.Stop)
    parent1.send(newEntity, AuthorizedCommand(cmdGetEntityParent1, None))
    parent1.expectNoMessage()
    parent1.ref ! PoisonPill

    val parent2 = TestProbe("RecoveredEntity")
    val cmdGetEntityParent2 = AuthorizedCommand(GetEntity(CommandId(), entity.id, parent2.ref), None)
    val recoveredEntity = parent2.childActorOf(TestValueObjectPersistentActor.props(entity.id),
      EntityPersistentActor.entityPersistenceId(classOf[TestValueObject], entity.id))
    parent2.send(recoveredEntity, cmdGetEntityParent2)
    parent2.expectMsgPF(10 seconds) {
      case TestValueObject(id, count) =>
        assert(id.value == entity.id.value)
        //assert(count == entity.count + 5 + 5 - 5)
    }
  }
}
