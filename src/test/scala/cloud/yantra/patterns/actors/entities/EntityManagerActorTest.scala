/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.entities

import akka.actor.ActorSystem
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Subscribe
import akka.testkit.{TestKit, TestProbe}
import cloud.yantra.patterns.actors.FrameworkActorTest
import cloud.yantra.patterns.actors.entities.EntityManagerActor.Commands.AuthorizedCommand
import cloud.yantra.patterns.actors.entities.EntityPersistentActor.Commands
import cloud.yantra.patterns.actors.entities.TestValueObjectPersistentActor.Commands.{Create, DecrementCount, IncrementCount}
import cloud.yantra.patterns.actors.entities.TestValueObjectPersistentActor.Events.{TestValueObjectCountDecreased, TestValueObjectCountIncreased, TestValueObjectCreated}
import cloud.yantra.patterns.cqrs.CommandId
import cloud.yantra.patterns.es.{EntityId, EntityUuid}

import scala.concurrent.duration._
import scala.language.postfixOps

class EntityManagerActorTest
  extends TestKit(ActorSystem.create(classOf[EntityManagerActorTest].getSimpleName))
    with FrameworkActorTest {

  "Entity manager" must
    "not create entities when an unauthorized create command is received" in {
    val bot = TestProbe("EntityManagerProbe")
    val mediator = DistributedPubSub(system).mediator
    val topic = classOf[TestValueObjectPersistentActor.Events.TestValueObjectCreated].getCanonicalName
    mediator ! Subscribe(topic, bot.ref)

    val cmdCreateE1 = Create(CommandId(), 10)
    val entityManager = bot.childActorOf(TestValueObjectManagerActor.props())
    bot.send(entityManager, cmdCreateE1)
    bot.expectNoMessage()
  }

  it must "create entities when a create command is received" in {
    val bot = TestProbe("EntityManagerProbe")
    val mediator = DistributedPubSub(system).mediator
    val topic = classOf[TestValueObjectPersistentActor.Events.TestValueObjectCreated].getCanonicalName
    mediator ! Subscribe(topic, bot.ref)

    val cmdCreateE1 = Create(CommandId(), 10)
    val entityManager = bot.childActorOf(TestValueObjectManagerActor.props())
    bot.send(entityManager, AuthorizedCommand(cmdCreateE1, None))
    bot.expectMsgPF() {
      case TestValueObjectCreated(_, _, count) =>
        assert(count == 10)
    }
  }

  it must "be able to send update commands to entities post creation" in {
    val bot = TestProbe("EntityManagerProbe")
    val mediator = DistributedPubSub(system).mediator
    val topic = classOf[TestValueObjectPersistentActor.Events.TestValueObjectCreated].getCanonicalName
    mediator ! Subscribe(topic, bot.ref)
    mediator ! Subscribe(classOf[TestValueObject].getSimpleName, bot.ref)

    val cmdCreateE1 = Create(CommandId(), 10)
    val entityManager = bot.childActorOf(TestValueObjectManagerActor.props())
    var entityId: EntityId = EntityUuid()
    bot.send(entityManager, AuthorizedCommand(cmdCreateE1, None))
    bot.expectMsgPF() {
      case TestValueObjectCreated(_, entId, count) =>
        assert(count == 10)
        entityId = entId
      case unknown =>
        assert(false, s"Did not receive [TestValueObjectCreated] message. Received [${unknown}].")
    }

    val cmdIncrementE1 = IncrementCount(CommandId(), entityId, 5)
    bot.send(entityManager, AuthorizedCommand(cmdIncrementE1, None))
    bot.expectMsgPF() {
      case TestValueObjectCountIncreased(_, entId, countIncreasedBy) =>
        assert(entId == entityId)
        assert(countIncreasedBy == 5)
      case unknown =>
        assert(false, s"Did not receive [TestValueObjectCountIncreased] message. Received [${unknown}].")
    }
  }

  it must "create more than one entities" in {
    val bot = TestProbe("EntityManagerProbe")
    val mediator = DistributedPubSub(system).mediator
    val topic = classOf[TestValueObjectPersistentActor.Events.TestValueObjectCreated].getCanonicalName
    mediator ! Subscribe(topic, bot.ref)

    val cmdCreateE1 = Create(CommandId(), 10)
    val cmdCreateE2 = Create(CommandId(), 20)
    val entityManager = bot.childActorOf(TestValueObjectManagerActor.props())

    bot.send(entityManager, AuthorizedCommand(cmdCreateE1, None))
    bot.expectMsgPF() {
      case TestValueObjectCreated(_, entityId, count) =>
        assert(count == 10)
      case _ =>
        assert(false, "Did not receive [TestValueObjectCreated] message.")
    }

    bot.send(entityManager, AuthorizedCommand(cmdCreateE2, None))
    bot.expectMsgPF() {
      case TestValueObjectCreated(_, _, count) =>
        assert(count == 20)
      case _ =>
        assert(false, "Did not receive [TestValueObjectCreated] message.")
    }
  }

  it must "be able to pass details of single entity when queried " in {
    val bot = TestProbe("EntityManagerProbe")
    val mediator = DistributedPubSub(system).mediator
    val topic = classOf[TestValueObjectPersistentActor.Events.TestValueObjectCreated].getCanonicalName
    mediator ! Subscribe(topic, bot.ref)

    val cmdCreateE1 = Create(CommandId(), 10)

    val entityManager = bot.childActorOf(TestValueObjectManagerActor.props())
    bot.send(entityManager, AuthorizedCommand(cmdCreateE1, None))
    var entityIdE1: EntityId = EntityUuid()

    bot.expectMsgPF() {
      case TestValueObjectCreated(_, entityId, count) =>
        assert(count == 10)
        entityIdE1 = entityId
        val cmdGetDetail = Commands.GetEntity(CommandId(), entityId, bot.ref)
        bot.send(entityManager, AuthorizedCommand(cmdGetDetail, None))
      case _ =>
        assert(false, "Did not receive [TestValueObjectCreated] message.")
    }
    bot.expectMsgPF() {
      case TestValueObject(entId, cnt) =>
        assert(entId == entityIdE1)
        assert(cnt == 10)
      case _ =>
        assert(false, "Did not receive [TestValueObject] entity detail for E1.")
    }
  }

  it must "be able to pass details of multiple entities when queried " in {
    val bot = TestProbe("EntityManagerProbe")
    val mediator = DistributedPubSub(system).mediator
    val topic = classOf[TestValueObjectPersistentActor.Events.TestValueObjectCreated].getCanonicalName
    mediator ! Subscribe(topic, bot.ref)

    val cmdCreateE1 = Create(CommandId(), 10)
    val cmdCreateE2 = Create(CommandId(), 20)

    var entityIdE1: EntityId = EntityUuid()
    var entityIdE2: EntityId = EntityUuid()

    val entityManager = bot.childActorOf(TestValueObjectManagerActor.props())

    bot.send(entityManager, AuthorizedCommand(cmdCreateE1, None))
    bot.expectMsgPF() {
      case TestValueObjectCreated(_, entityId, count) =>
        assert(count == 10)
        entityIdE1 = entityId
      case _ =>
        assert(false, "Did not receive [TestValueObjectCreated] message.")
    }

    bot.send(entityManager, AuthorizedCommand(cmdCreateE2, None))
    bot.expectMsgPF() {
      case TestValueObjectCreated(_, entityId, count) =>
        assert(count == 20)
        entityIdE2 = entityId
      case _ =>
        assert(false, "Did not receive [TestValueObjectCreated] message.")
    }

    val cmdGetDetailE1 = Commands.GetEntity(CommandId(), entityIdE1, bot.ref)
    bot.send(entityManager, AuthorizedCommand(cmdGetDetailE1, None))
    bot.expectMsgPF() {
      case TestValueObject(entId, cnt) =>
        assert(entId == entityIdE1)
        assert(cnt == 10)
      case _ =>
        assert(false, "Did not receive [TestValueObject] entity detail for E1.")
    }

    val cmdGetDetailE2 = Commands.GetEntity(CommandId(), entityIdE2, bot.ref)
    bot.send(entityManager, AuthorizedCommand(cmdGetDetailE2, None))
    bot.expectMsgPF() {
      case TestValueObject(entId, cnt) =>
        assert(entId == entityIdE2)
        assert(cnt == 20)
      case _ =>
        assert(false, "Did not receive [TestValueObject] entity detail for E2.")
    }
  }

  it must "must be able able to update multiple entities" in {
    val bot = TestProbe("EntityManagerProbe")
    val createTopic = classOf[TestValueObjectPersistentActor.Events.TestValueObjectCreated].getCanonicalName
    val updateTopic = classOf[TestValueObject].getSimpleName
    val mediator = DistributedPubSub(system).mediator
    mediator ! Subscribe(createTopic, bot.ref)
    mediator ! Subscribe(updateTopic, bot.ref)

    val cmdCreateE1 = Create(CommandId(), 10)
    val cmdCreateE2 = Create(CommandId(), 20)
    // Using var to capture entityId received after successful creation of entities
    var entityIdE1: EntityId = EntityUuid()
    var entityIdE2: EntityId = EntityUuid()

    val entityManager = bot.childActorOf(TestValueObjectManagerActor.props())

    bot.send(entityManager, AuthorizedCommand(cmdCreateE1, None))
    bot.expectMsgPF(5 seconds, "Object creation with count 10, E1") {
      case TestValueObjectCreated(_, entityId, count) =>
        assert(count == 10)
        entityIdE1 = entityId
      case _ =>
        assert(false, "Did not receive [TestValueObjectCreated] message.")
    }

    bot.send(entityManager, AuthorizedCommand(cmdCreateE2, None))
    bot.expectMsgPF(5 seconds, "Object creation with count 20, E2") {
      case TestValueObjectCreated(_, entityId, count) =>
        assert(count == 20)
        entityIdE2 = entityId
      case unknown =>
        assert(false, s"Did not receive [TestValueObjectCreated] message, and received [${unknown}].")
    }

    val cmdIncrementE1 = IncrementCount(CommandId(), entityIdE1, 5)
    bot.send(entityManager, AuthorizedCommand(cmdIncrementE1, None))
    bot.expectMsgPF(5 seconds, "Count increase by 5 , by entity E1") {
      case TestValueObjectCountIncreased(_, entId, cnt) =>
        assert(entId == entityIdE1)
        assert(cnt == 5)
      case _ =>
        assert(false, "Did not receive [TestValueObjectCountIncreased] entity detail for E1.")
    }

    val cmdDecrementE2 = DecrementCount(CommandId(), entityIdE2, 5)
    bot.send(entityManager, AuthorizedCommand(cmdDecrementE2, None))
    bot.expectMsgPF(5 seconds, "Count decreased by 5, by entitty E2") {
      case TestValueObjectCountDecreased(_, entId, cnt) =>
        assert(entId == entityIdE2)
        assert(cnt == 5)
      case _ =>
        assert(false, "Did not receive [TestValueObjectCountDecreased] entity detail for E2.")
    }
  }
}
