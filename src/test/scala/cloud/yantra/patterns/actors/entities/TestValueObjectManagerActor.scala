/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.entities

import akka.actor.Props
import cloud.yantra.patterns.es.EntityId

class TestValueObjectManagerActor()
  extends EntityManagerActor[TestValueObject](classOf[TestValueObject]) {

  override def subscribeToValidatedCommandTopics: Seq[String] = Seq(classOf[TestValueObjectManagerActor].getSimpleName)


  override def publishToDomainEventTopics: Seq[String] = Nil
  override def entityProps(entityId: EntityId): Props = TestValueObjectPersistentActor.props(entityId)
}

object TestValueObjectManagerActor {
  def props(): Props = Props(new TestValueObjectManagerActor())
}
