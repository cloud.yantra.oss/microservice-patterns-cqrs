/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors

import akka.testkit.TestKit
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

trait FrameworkActorTest
  extends FlatSpecLike
    with Matchers
    with BeforeAndAfterAll {
  testKit: TestKit =>

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(testKit.system)
  }

}
