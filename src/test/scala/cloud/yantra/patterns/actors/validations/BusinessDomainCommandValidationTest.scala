/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.validations

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import cloud.yantra.patterns.actors.FrameworkActorTest
import cloud.yantra.patterns.actors.validations.CommandValidation._
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage

import scala.concurrent.duration._
import scala.language.postfixOps

class BusinessDomainCommandValidationTest
  extends TestKit(ActorSystem.create(classOf[BusinessDomainCommandValidationTest].getSimpleName))
    with FrameworkActorTest {

  case class TestRule(result: Result) extends Rule {
    override val id: RuleId = RuleIdUuid()

    override val validate: ValidationRule = () => result
  }

  case class TestCommand(
                          id: CommandId = CommandUuid(),
                          property: String = "Some test property",
                        ) extends Command

  case class TBusinessCommandValidation(commandId: CommandId,
                                        rules: Seq[Rule])
    extends BusinessDomainCommandValidation[TestCommand](commandId)

  "Business validation of Command " must
    "return validation result as Passed, when every rule has passed" in {
    val parent = TestProbe()
    val cmd = TestCommand()
    val valId = cmd.id
    val businessDomainCommandValidation = parent.childActorOf(Props(
      new TBusinessCommandValidation(
        valId,
        Seq(TestRule(Passed), TestRule(Passed)))))
    val validationCompleted = ValidationCompleted(
      valId.value,
      DomainProcessingStage.BusinessValidation,
      cmd,
      Passed
    )
    parent.send(businessDomainCommandValidation, ValidateCommand(cmd))
    parent.expectMsg(CommandValidationInitiated(valId.value, DomainProcessingStage.BusinessValidation, cmd))

    parent.ignoreMsg({
      case ValidationInProgress(_, DomainProcessingStage.BusinessValidation, _, _) => true
    })

    parent.send(businessDomainCommandValidation, GetCommandValidationStatus(parent.ref))
    within(5 seconds) {
      parent.send(businessDomainCommandValidation, GetCommandValidationStatus(parent.ref))
      expectNoMessage
      parent.expectMsgPF(3 seconds) {
        case completed: ValidationCompleted => assert(validationCompleted == completed)
        case unknown => fail(s"Did not receive neither ValidationInProgress or Completed, instead [${unknown}]")
      }
    }
  }

  it must "return validation result as Passed, when the rule set is empty" in {
    val parent = TestProbe()
    val cmd = TestCommand()
    val valId = cmd.id
    val businessDomainCommandValidation = parent.childActorOf(Props(
      new TBusinessCommandValidation(
        valId,
        Nil)))
    val validationCompleted = ValidationCompleted(
      valId.value,
      DomainProcessingStage.BusinessValidation,
      cmd,
      Passed
    )
    parent.send(businessDomainCommandValidation, ValidateCommand(cmd))
    parent.expectMsg(CommandValidationInitiated(valId.value, DomainProcessingStage.BusinessValidation, cmd))

    parent.ignoreMsg({
      case ValidationInProgress(_, DomainProcessingStage.BusinessValidation, _, _) => true
    })

    within(5 seconds) {
      expectNoMessage
      parent.expectMsgPF(3 seconds) {
        case completed: ValidationCompleted => assert(validationCompleted == completed)
        case unknown => fail(s"Did not receive neither ValidationInProgress or Completed, instead [${unknown}]")
      }
    }
  }
}
