/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.validations

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import cloud.yantra.patterns.actors.FrameworkActorTest
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.errors.DomainError

class ValidationRuleWorkerTest
  extends TestKit(ActorSystem("ValidationRuleWorkerTest"))
    with FrameworkActorTest {

  case class TestRule(result: Result) extends Rule {
    override def id: RuleId = RuleIdUuid()

    override def validate: ValidationRule = () => result
  }

  case object TestError extends DomainError {
    override val code: String = "DETE1"

    override val message: String = "Domain technical validation error 1"

    override val localizationMessageParameters: Seq[String] = Nil
  }

  "Validation rule worker actor" must
    "NOT validate until initialization message is received" in {
    val parent = TestProbe()
    val testRule = new TestRule(Passed)
    val child = parent.childActorOf(ValidationRuleWorker.props(testRule, parent.ref))
    parent.send(child, ValidationRuleWorker.GetValidationResult())
    parent.expectMsg(ValidationRuleWorker.WorkerWaitingInitiation(testRule))
  }
  it must "provides Passed result when initialization message is sent, and rule passes" in {
    val parent = TestProbe()
    val testRule = new TestRule(Passed)
    val child = parent.childActorOf(ValidationRuleWorker.props(testRule, parent.ref))
    parent.send(child, ValidationRuleWorker.InitiateValidation)
    parent.expectMsg(ValidationRuleWorker.WorkerFinishedValidation(testRule, Passed))
  }
  it must "provides Failed result when initialization message is sent, and rule fails" in {
    val parent = TestProbe()
    val failed = Result(TestError)
    val testRule = new TestRule(failed)
    val child = parent.childActorOf(ValidationRuleWorker.props(testRule, parent.ref))
    parent.send(child, ValidationRuleWorker.InitiateValidation)
    parent.expectMsg(ValidationRuleWorker.WorkerFinishedValidation(testRule, failed))
  }

}
