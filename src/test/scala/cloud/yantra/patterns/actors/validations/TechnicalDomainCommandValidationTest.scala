/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.validations

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import cloud.yantra.patterns.actors.FrameworkActorTest
import cloud.yantra.patterns.actors.validations.CommandValidation._
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage

import scala.concurrent.duration._
import scala.language.postfixOps

class TechnicalDomainCommandValidationTest
  extends TestKit(ActorSystem("TechnicalDomainCommandValidationTest"))
    with FrameworkActorTest {

  case class TestRule(result: Result) extends Rule {
    override val id: RuleId = RuleIdUuid()

    override val validate: ValidationRule = () => result
  }

  case class TestCommand(
                          id: CommandId = CommandUuid(),
                          property: String = "Some test property",
                        ) extends Command

  case class TTechnicalCommandValidation(commandId: CommandId,
                                         rules: Seq[Rule])
    extends TechnicalDomainCommandValidation[TestCommand](commandId)

  "Technical validation of Command " must
    "return validation result as Passed, when every rule has passed" in {
    val parent = TestProbe()
    val cmd = TestCommand()
    val techValId = cmd.id
    val technicalDomainCommandValidation = parent.childActorOf(Props(
      new TTechnicalCommandValidation(
        techValId,
        Seq(TestRule(Passed), TestRule(Passed)))))
    val validationCompleted = ValidationCompleted(
      techValId.value,
      DomainProcessingStage.TechnicalValidation,
      cmd,
      Passed
    )
    parent.send(technicalDomainCommandValidation, ValidateCommand(cmd))
    parent.expectMsg(CommandValidationInitiated(techValId.value, DomainProcessingStage.TechnicalValidation, cmd))
    parent.ignoreMsg({
      case ValidationInProgress(_, DomainProcessingStage.BusinessValidation, _, _) => true
    })
    parent.send(technicalDomainCommandValidation, GetCommandValidationStatus(parent.ref))
    within(10 seconds) {
      expectNoMessage
      parent.expectMsg(validationCompleted)
    }
  }
  it must "return validation result as Passed, when rule set is empty" in {
    val parent = TestProbe()
    val cmd = TestCommand()
    val techValId = cmd.id
    val technicalDomainCommandValidation = parent.childActorOf(Props(
      new TTechnicalCommandValidation(
        techValId,
        Nil)))
    val validationCompleted = ValidationCompleted(
      techValId.value,
      DomainProcessingStage.TechnicalValidation,
      cmd,
      Passed
    )
    parent.send(technicalDomainCommandValidation, ValidateCommand(cmd))
    parent.expectMsg(CommandValidationInitiated(techValId.value, DomainProcessingStage.TechnicalValidation, cmd))
    parent.ignoreMsg({
      case ValidationInProgress(_, DomainProcessingStage.BusinessValidation, _, _) => true
    })
    within(10 seconds) {
      expectNoMessage
      parent.expectMsg(validationCompleted)
    }
  }
}
