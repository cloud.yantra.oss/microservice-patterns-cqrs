/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.actors.validations

import akka.actor.{ActorSystem, PoisonPill, Props}
import akka.testkit.{TestKit, TestProbe}
import cloud.yantra.patterns.actors.FrameworkActorTest
import cloud.yantra.patterns.actors.validations.CommandValidation._
import cloud.yantra.patterns.cqrs._
import cloud.yantra.patterns.cqrs.errors.DomainError
import cloud.yantra.patterns.cqrs.stages.DomainProcessingStage

import scala.concurrent.duration._
import scala.language.postfixOps

class CommandValidationTest
  extends TestKit(ActorSystem.create("CommandValidationTest"))
    with FrameworkActorTest {

  case class TestRule(result: Result) extends Rule {
    override val id: RuleId = RuleIdUuid()

    override val validate: ValidationRule = () => result
  }

  case object TestError1 extends DomainError {
    override val code: String = "TestError1"

    override val message: String = "Domain technical validation error 1"

    override val localizationMessageParameters: Seq[String] = Nil
  }

  case object TestError2 extends DomainError {
    override val code: String = "TestError2"

    override val message: String = "Domain technical validation error 2"

    override val localizationMessageParameters: Seq[String] = Nil
  }

  case class TestCommand(
                          id: CommandId = CommandUuid(),
                          property: String = "Some test property"
                        ) extends Command

  case class TestCommandValidation(override val commandValidationId: String,
                                   rules: Seq[Rule],
                                   domainProcessingStage: DomainProcessingStage = DomainProcessingStage.TechnicalValidation)
    extends CommandValidation[TestCommand]

  "Validation of Command " must
    "should start with empty command" in {
    val parent = TestProbe()
    val cmd = TestCommand(id = CommandUuid())
    val technicalDomainCommandValidation = parent.childActorOf(Props(
      new TestCommandValidation(
        cmd.id.value,
        Seq(TestRule(Passed), TestRule(Passed)))))
    parent.send(technicalDomainCommandValidation, GetCommandValidationStatus(parent.ref))
    parent.expectMsgType[NoCommandAvailableForValidation]
  }

  it must "accept only 1 command for processing" in {
    val parent1 = TestProbe()
    val parent2 = TestProbe()
    val cmd = TestCommand(id = CommandUuid())
    val techValId = cmd.id.value
    val technicalDomainCommandValidation = parent1.childActorOf(Props(
      new TestCommandValidation(
        techValId,
        Seq(TestRule(Passed), TestRule(Passed)))))
    parent1.send(technicalDomainCommandValidation, ValidateCommand(cmd))
    parent1.expectMsg(CommandValidationInitiated(techValId, DomainProcessingStage.TechnicalValidation, cmd))
    parent2.send(technicalDomainCommandValidation, ValidateCommand(cmd))
    parent2.expectMsg(UnavailableForCommandValidation(techValId, DomainProcessingStage.TechnicalValidation))
  }

  it must "consider rules of similar nature as different" in {
    val r1 = TestRule(Passed)
    val r2 = TestRule(Passed)
    assert(r1.id != r2.id)
  }

  it must "start processing of command when command is sent for validation" in {
    val parent = TestProbe()
    val cmd = TestCommand(id = CommandUuid())
    val techValId = cmd.id.value
    val ruleList = Seq.fill[Rule](10)(
      new Rule() {
        override val id: RuleId = RuleIdUuid()

        override def validate: ValidationRule = () => {
          Thread.sleep(1000)
          Passed
        }
      })
    val technicalDomainCommandValidation = parent.childActorOf(Props(
      new TestCommandValidation(techValId, ruleList)))
    parent.send(technicalDomainCommandValidation, ValidateCommand(cmd))
    parent.expectMsg(CommandValidationInitiated(techValId, DomainProcessingStage.TechnicalValidation, cmd))
    parent.send(technicalDomainCommandValidation, GetCommandValidationStatus(parent.ref))

    val validationInProgress = ValidationInProgress(
      techValId,
      DomainProcessingStage.TechnicalValidation,
      cmd,
      NotEvaluated)
    parent.expectMsgAnyClassOf(validationInProgress.getClass)
    // Kill actor, as test is complete
    technicalDomainCommandValidation ! PoisonPill
  }

  it must "return validation result as Passed, when rule set is empty" in {
    val parent = TestProbe("ValidationPassed")
    val cmd = TestCommand(id = CommandUuid())
    val techValId = cmd.id.value
    val technicalDomainCommandValidation = parent.childActorOf(Props(
      new TestCommandValidation(
        techValId,
        Nil)))
    val validationCompleted = ValidationCompleted(
      techValId,
      DomainProcessingStage.TechnicalValidation,
      cmd,
      Passed
    )
    parent.send(technicalDomainCommandValidation, ValidateCommand(cmd))
    parent.expectMsg(5 seconds,
      CommandValidationInitiated(techValId, DomainProcessingStage.TechnicalValidation, cmd))

    parent.ignoreMsg({
      case ValidationInProgress(_, DomainProcessingStage.BusinessValidation, _, _) => true
    })

    parent.send(technicalDomainCommandValidation, GetCommandValidationStatus(parent.ref))
    within(5 seconds) {
      expectNoMessage
      parent.expectMsg(validationCompleted)
    }
  }

  it must "return validation result as Passed, when every rule has passed" in {
    val parent = TestProbe("ValidationPassed")
    val cmd = TestCommand(id = CommandUuid())
    val techValId = cmd.id.value
    val technicalDomainCommandValidation = parent.childActorOf(Props(
      new TestCommandValidation(
        techValId,
        Seq(TestRule(Passed), TestRule(Passed)))))
    val validationCompleted = ValidationCompleted(
      techValId,
      DomainProcessingStage.TechnicalValidation,
      cmd,
      Passed
    )
    parent.send(technicalDomainCommandValidation, ValidateCommand(cmd))
    parent.expectMsg(5 seconds,
      CommandValidationInitiated(techValId, DomainProcessingStage.TechnicalValidation, cmd))

    parent.ignoreMsg({
      case ValidationInProgress(_, DomainProcessingStage.BusinessValidation, _, _) => true
    })

    parent.send(technicalDomainCommandValidation, GetCommandValidationStatus(parent.ref))
    within(10 seconds) {
      expectNoMessage
      parent.expectMsg(validationCompleted)
    }
  }

  it must "return validation result as Failed, when at least one rule has Failed" in {
    val parent = TestProbe()
    val cmd = TestCommand(id = CommandUuid())
    val techValId = cmd.id.value
    val technicalDomainCommandValidation = parent.childActorOf(Props(
      new TestCommandValidation(
        techValId,
        Seq(TestRule(Passed), TestRule(Result(TestError1))))))
    val validationCompleted = ValidationCompleted(
      techValId,
      DomainProcessingStage.TechnicalValidation,
      cmd,
      Result(TestError1)
    )
    parent.send(technicalDomainCommandValidation, ValidateCommand(cmd))
    parent.expectMsg(CommandValidationInitiated(techValId, DomainProcessingStage.TechnicalValidation, cmd))

    parent.ignoreMsg({
      case ValidationInProgress(_, DomainProcessingStage.BusinessValidation, _, _) => true
    })

    parent.send(technicalDomainCommandValidation, GetCommandValidationStatus(parent.ref))
    within(2000 millis) {
      expectNoMessage
      parent.expectMsg(validationCompleted)
    }
  }

  it must "return validation result as Failed, and return all Errors" in {
    val parent = TestProbe()
    val cmd = TestCommand(id = CommandUuid())
    val techValId = cmd.id.value
    val technicalDomainCommandValidation = parent.childActorOf(Props(
      new TestCommandValidation(
        techValId,
        Seq(TestRule(Result(TestError2)), TestRule(Result(TestError1))))))
    val validationCompleted = ValidationCompleted(
      techValId,
      DomainProcessingStage.TechnicalValidation,
      cmd,
      Failed(Seq(TestError2, TestError1))
    )

    parent.send(technicalDomainCommandValidation, ValidateCommand(cmd))
    parent.expectMsg(CommandValidationInitiated(techValId, DomainProcessingStage.TechnicalValidation, cmd))

    parent.ignoreMsg({
      case ValidationInProgress(_, DomainProcessingStage.BusinessValidation, _, _) => true
    })

    parent.send(technicalDomainCommandValidation, GetCommandValidationStatus(parent.ref))
    within(2000 millis) {
      expectNoMessage
      parent.expectMsg(validationCompleted)
    }
  }
}
