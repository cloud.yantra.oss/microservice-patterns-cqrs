/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package cloud.yantra.patterns.es

import org.scalatest.{FlatSpecLike, Matchers}

class AggregateTest extends FlatSpecLike
  with Matchers {

  "Aggregate " must
    "provide aggregate id as entity id" in {
    val aggregate = new Aggregate {
      override val aggregateId: AggregateId = AggregateId()

      override def entity: String = "TestAggregate"
    }
    assert(aggregate.aggregateId == aggregate.id)
  }
}
