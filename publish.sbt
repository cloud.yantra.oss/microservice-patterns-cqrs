/** ****************************************************************************
  * OSSRH - Sonatype related configurations
  *
  * Deploy snapshot artifacts into repository https://oss.sonatype.org/content/repositories/snapshots
  * Deploy release artifacts into the staging repository https://oss.sonatype.org/service/local/staging/deploy/maven2
  * Promote staged artifacts into repository 'Releases'
  * Download snapshot and release artifacts from group https://oss.sonatype.org/content/groups/public
  * Download snapshot, release and staged artifacts from staging group https://oss.sonatype.org/content/groups/staging
  *
  * ****************************************************************************
  */

organization := sys.env.getOrElse("NEXUS_REPOSITORY_ORGANIZATION", "cloud.yantra.oss")
organizationName := sys.env.getOrElse("NEXUS_REPOSITORY_ORGANIZATION_NAME", "Yantra Cloud Ltd - OSS")
organizationHomepage := Some(url("https://yantra.cloud"))

scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/cloud.yantra.oss/microservice-patterns-cqrs.git"),
    "git@gitlab.com:cloud.yantra.oss/microservice-patterns-cqrs.git"
  )
)
developers := List(
  Developer(
    id = "pratimsc",
    name = "Pratim Chaudhuri",
    email = "pratim@chaudhuri.me",
    url = url("https://gitlab.com/pratimsc")
  )
)

description := "Some descripiton about your project."
licenses := List("MIT" -> new URL("https://opensource.org/licenses/MIT"))
homepage := Some(url("https://gitlab.com/cloud.yantra.oss/microservice-patterns-cqrs"))

// Remove all additional repository other than Maven Central from POM
pomIncludeRepository := { _ => false }
publishTo := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
  else Some("releases" at nexus + "service/local/staging/deploy/maven2")
}
publishMavenStyle := true

credentials += Credentials(realm = "Sonatype Nexus Repository Manager",
  host = "oss.sonatype.org",
  userName = sys.env.getOrElse("NEXUS_REPOSITORY_USERNAME", "admin"),
  passwd = sys.env.getOrElse("NEXUS_REPOSITORY_PASSWD", "admin")
)

// Ref: https://github.com/xerial/sbt-sonatype
sonatypeProfileName := organization.value
publishArtifact in(Test, packageBin) := false
publishArtifact in(Test, packageDoc) := false
