// The Typesafe repository
resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/maven-releases/"

// for autoplugins
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.6" withSources())

// Java checkstyle
addSbtPlugin("com.etsy" % "sbt-checkstyle-plugin" % "3.1.1" withSources())

dependencyOverrides += "com.puppycrawl.tools" % "checkstyle" % "8.12" withSources()

// Scala checkstyle
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0" withSources())

// Test coverage
// Ref: https://www.lunatech.com/blog/V5tpoCQAANsPL4jw/continuous-integration-on-gitlab-with-scala-and-sbt
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1" withSources())

// Sonatype plugin
addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "2.3" withSources())
addSbtPlugin("com.jsuereth" % "sbt-pgp" % "1.1.0" withSources())
