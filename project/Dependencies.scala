import sbt._

object Dependencies {

  lazy val slfjVersion = "1.7.25"
  lazy val scalatestVersion = "3.0.5"
  lazy val scalamockVersion = "4.1.0"
  lazy val akkaVersion = "2.5.18"
  lazy val inmemoryDbVersion = "2.5.15.1"
  lazy val akkaHttpVersion = "10.1.5"
  lazy val playJsonVersion = "2.6.10"

  //Libraries for Compiling
  val slf4jApi = "org.slf4j" % "slf4j-api" % slfjVersion
  val slf4jImpl = "org.slf4j" % "slf4j-simple" % slfjVersion

  //Libraries for Actor
  val akkaActor = "com.typesafe.akka" %% "akka-actor" % akkaVersion
  val akkaPersistent = "com.typesafe.akka" %% "akka-persistence" % akkaVersion
  val akkaRemote = "com.typesafe.akka" %% "akka-remote" % akkaVersion
  val akkaCluster = "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion
  val akkaHttp = "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion
  val playJson = "com.typesafe.play" %% "play-json" % playJsonVersion

  //Libraries for Testing
  val scalatest = "org.scalatest" %% "scalatest" % scalatestVersion % Test
  val akkaTestkit = "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test
  val scalamock = "org.scalamock" %% "scalamock" % scalamockVersion % Test
  val inmemoryDb = "com.github.dnvriend" %% "akka-persistence-inmemory" % inmemoryDbVersion % Test
  val leveldb = "org.iq80.leveldb" % "leveldb" % "0.10" % Test
  val leveldbjni = "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8" % Test

  //Resolvers
  val resolvers = Seq("bintray-dnvriend-maven" at "https://dl.bintray.com/dnvriend/maven")
  //Projects
  val groupBackendDependencies = Seq(
    akkaActor,
    akkaPersistent,
    akkaRemote,
    akkaCluster,
    playJson,
    slf4jApi,
    slf4jImpl,
    scalatest, akkaTestkit, scalamock, inmemoryDb, leveldb, leveldbjni)
}
